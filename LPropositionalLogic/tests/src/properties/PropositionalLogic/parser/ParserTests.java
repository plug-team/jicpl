package properties.PropositionalLogic.parser;

import org.junit.Test;
import properties.PropositionalLogic.PropositionalLogicModel.*;
import properties.PropositionalLogic.transformations.PrettyPrinter;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

public class ParserTests {
	Expression exp;
	
	@Test
	public void testPP() {
		exp = Parser.parse("let \n" +
                "\t\tx\t\t= fiacre| control.a && toto |,  \n" +
                "\t\ty\t\t= cdl\t| 23 |,  \n" +
                "\t\tlkfld\t=\t\t| jkjk |  \n" +
                "\tin  \n" +
                "\t\tlet z = x and y in \n" +
                "\t\t\ttrue and false or z -> |tot| -> x");
		
		String expected = "let\n" +
                "\tx = fiacre| control.a && toto |\n" +
                "\ty = cdl| 23 |\n" +
                "\tlkfld = | jkjk |\n" +
                "in\n" +
                "\tlet\n" +
                "\t\tz = (x and y)\n" +
                "\tin\n" +
                "\t\t(((true and false) or z) -> (|tot| -> x))";
		String result = PrettyPrinter.print(exp);
		assertThat(result, is(expected));
	}
 	
	@Test
	public void testReferenceToBlockDecl() {
		DeclarationBlock block = Parser.parseBlock("a = true b = a");
		assertNotNull(block);
		assertThat(block.getDeclarations().size(), is(2));
		ExpressionDeclaration b = (ExpressionDeclaration) block.getDeclarations().get(1);
		assertThat(b.getExpression(), is(instanceOf(ExpressionReference.class)));
		ExpressionDeclaration a = (ExpressionDeclaration) block.getDeclarations().get(0);
		ExpressionReference toA = (ExpressionReference)b.getExpression();
		assertThat(toA.getExp(), is(sameInstance(a)));
	}
	
	@Test
	public void testParseBlock() {
		DeclarationBlock block = Parser.parseBlock("a = true b=let a = false in a");
		assertNotNull(block);
		assertThat(block.getDeclarations().size(), is(2));
		ExpressionDeclaration decl = (ExpressionDeclaration) block.getDeclarations().get(0);
		assertThat(decl.getName(), is("a"));
		assertThat(decl.getExpression(), is(instanceOf(True.class)));
		decl = (ExpressionDeclaration) block.getDeclarations().get(1);
		assertThat(decl.getName(), is("b"));
		assertThat(decl.getExpression(), is(instanceOf(LetExpression.class)));
		LetExpression let = (LetExpression)decl.getExpression();
		assertThat(let.getDeclarations().size(), is(1));
		ExpressionDeclaration a = (ExpressionDeclaration) let.getDeclarations().get(0);
		assertThat(a.getExpression(), is(instanceOf(False.class)));
		assertThat(let.getExpression(), is(instanceOf(ExpressionReference.class)));
		ExpressionReference ref = (ExpressionReference)let.getExpression();
		assertThat(ref.getName(), is("a"));
		assertThat(ref.getExp(), is(sameInstance(a)));
	
	}
	
	@Test
	public void testParseLet() {
		exp = Parser.parse("let a = false in a + true && |2| -> 0");
		assertNotNull(exp);
		assertThat(exp, is(instanceOf(LetExpression.class)));
		LetExpression let = (LetExpression)exp;
		assertThat(let.getDeclarations().size(), is(1));
		assertThat(let.getExpression(), is(instanceOf(LogicalImplication.class)));
	}
	
	@Test
	public void testParseConjunction() {
		assertBinary("&", LogicalConjunction.class);
	}
	
	@Test
	public void testParseDisjunction() {
		assertBinary("+", LogicalDisjunction.class);
	}
	
	@Test
	public void testParseXOR() {
		assertBinary("^", LogicalXOR.class);
	}
	
	@Test
	public void testParseImplication() {
		assertBinary("->", LogicalImplication.class);
	}
	
	@Test
	public void testParseEquivalence() {
		assertBinary("<=>", LogicalEquivalence.class);
	}
	
	private <T> void assertBinary(String operator, Class<T> type) {
		exp = Parser.parse("|x|"+operator+"false");
		assertThat(exp, is(instanceOf(type)));
		assertThat(((BinaryExpression)exp).getLhs(), is(instanceOf(Atom.class)));
		assertThat(((BinaryExpression)exp).getRhs(), is(instanceOf(False.class)));
	}
	
	@Test
	public void testParseNegation() {
		exp = Parser.parse("!|x|");
		assertThat(exp, is(instanceOf(LogicalNegation.class)));
		assertThat(((LogicalNegation)exp).getOperand(), is(instanceOf(Atom.class)));
	}
	
	@Test
	public void testParseParen() {
		exp = Parser.parse("(true)");
		assertThat(exp, is(instanceOf(True.class)));
	}
	
	@Test
	public void testParseAtoms() {
		exp = Parser.parse("|a|");
		assertNotNull(exp != null);
		assertTrue(exp instanceof Atom);
		assertNull(((Atom)exp).getLanguage());
		assertThat(((Atom)exp).getCode(), is("a"));
		
		exp = Parser.parse("fc|a|");
		assertNotNull(exp != null);
		assertTrue(exp instanceof Atom);
		assertThat(((Atom)exp).getLanguage(), is("fc"));
		assertThat(((Atom)exp).getCode(), is("a"));
		
		exp = Parser.parse("\"a\"");
		assertNotNull(exp != null);
		assertTrue(exp instanceof Atom);
		assertNull(((Atom)exp).getLanguage());
		assertThat(((Atom)exp).getCode(), is("a"));
		
		exp = Parser.parse("fc\"a\"");
		assertNotNull(exp != null);
		assertTrue(exp instanceof Atom);
		assertThat(((Atom)exp).getLanguage(), is("fc"));
		assertThat(((Atom)exp).getCode(), is("a"));
	}
	
	@Test
	public void testParseLiterals() {
		exp = Parser.parse("true");
		assert(exp != null);
		assert(exp instanceof True);
		
		exp = Parser.parse("false");
		assert(exp != null);
		assert(exp instanceof False);
	}
}
