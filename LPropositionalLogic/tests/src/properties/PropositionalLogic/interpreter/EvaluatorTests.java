package properties.PropositionalLogic.interpreter;

import org.junit.Before;
import org.junit.Test;
import properties.PropositionalLogic.PropositionalLogicModel.DeclarationBlock;
import properties.PropositionalLogic.PropositionalLogicModel.Expression;
import properties.PropositionalLogic.interpreter.atom.AtomArrayValuationEvaluator;
import properties.PropositionalLogic.interpreter.atom.AtomConstantMapEvaluator;
import properties.PropositionalLogic.parser.Parser;
import properties.PropositionalLogic.parser.SymbolResolver.SymbolMissingException;

import java.util.HashMap;
import java.util.Map;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

public class EvaluatorTests {
	Evaluator eval;
	
	@SuppressWarnings("serial")
	@Test
	public void testConstantMapAtomIsDefaultEvaluator() {
		Map<String, Boolean> constantMap = new HashMap<String, Boolean>() {{
			put("a", true);
			put("b", true);
			put("!a", false);
			put("!b", false);
		}};
		eval.addAtomEvaluator("cst", new AtomConstantMapEvaluator(constantMap));
		
		constantMap = new HashMap<String, Boolean>() {{
			put("toifk", true);
			put("a", true);
			put("b", true);
		}};
		eval.setDefaultEvaluator(new AtomConstantMapEvaluator(constantMap));
		
		assertThat(evaluate("|toifk|"), is(true));
		assertThat(evaluate("|a|"), is(true));
		assertThat(evaluate("|b|"), is(true));
		
		
		try {
			evaluate("|c|");
		} catch (SymbolMissingException e) {
			assertTrue(true);
		} catch (Exception e) {
			assertTrue(false);
		}
		
		
		assertThat(evaluate("cst|a|"), is(true));
		assertThat(evaluate("cst|b|"), is(true));
		try {
			evaluate("cst|c|");
		} catch (SymbolMissingException e) {
			assertTrue(true);
		} catch (Exception e) {
			assertTrue(false);
		}
		assertThat(evaluate("cst|a| xor cst|!b|"), is(true));
		assertThat(evaluate("cst|a| xor cst|b|"), is(false));
		assertThat(evaluate("cst|!a| xor cst|b|"), is(true));
		assertThat(evaluate("cst|!a| xor cst|!b|"), is(false));
	}

	@Test
	public void testArrayValuationAtomEvaluator() {
		Map<String, Integer> atomicPropositionTable = new HashMap<String, Integer>(){{
			put("a", 0);
			put("b", 1);
			put("!a", 2);
			put("!b", 3);
		}};

		AtomArrayValuationEvaluator apEvaluator = new AtomArrayValuationEvaluator(atomicPropositionTable);

		eval.addAtomEvaluator("cst", apEvaluator);

		apEvaluator.setValuation(new boolean[]{true, true, false, false});

		assertThat(evaluate("|toifk|"), is(false));
		assertThat(evaluate("|a|"), is(false));
		assertThat(evaluate("|b|"), is(false));
		assertThat(evaluate("cst|a|"), is(true));
		assertThat(evaluate("cst|b|"), is(true));
		assertThat(evaluate("cst|!a|"), is(false));
		assertThat(evaluate("cst|!b|"), is(false));
		try {
			evaluate("cst|c|");
		} catch (SymbolMissingException e) {
			assertTrue(true);
		} catch (Exception e) {
			assertTrue(false);
		}
		assertThat(evaluate("cst|a| xor cst|!b|"), is(true));
		assertThat(evaluate("cst|a| xor cst|b|"), is(false));
		assertThat(evaluate("cst|!a| xor cst|b|"), is(true));
		assertThat(evaluate("cst|!a| xor cst|!b|"), is(false));

		apEvaluator.setValuation(new boolean[]{false, false, true, true});
		assertThat(evaluate("|toifk|"), is(false));
		assertThat(evaluate("|a|"), is(false));
		assertThat(evaluate("|b|"), is(false));
		assertThat(evaluate("cst|a|"), is(false));
		assertThat(evaluate("cst|b|"), is(false));
		assertThat(evaluate("cst|!a|"), is(true));
		assertThat(evaluate("cst|!b|"), is(true));
	}

	@Test
	public void testConstantMapAtomEvaluator() {
		@SuppressWarnings("serial")
		Map<String, Boolean> constantMap = new HashMap<String, Boolean>() {{
			put("a", true);
			put("b", true);
			put("!a", false);
			put("!b", false);
		}};
		eval.addAtomEvaluator("cst", new AtomConstantMapEvaluator(constantMap));
		
		assertThat(evaluate("|toifk|"), is(false));
		assertThat(evaluate("|a|"), is(false));
		assertThat(evaluate("|b|"), is(false));
		assertThat(evaluate("cst|a|"), is(true));
		assertThat(evaluate("cst|b|"), is(true));
		try {
			evaluate("cst|c|");
		} catch (SymbolMissingException e) {
			assertTrue(true);
		} catch (Exception e) {
			assertTrue(false);
		}
		assertThat(evaluate("cst|a| xor cst|!b|"), is(true));
		assertThat(evaluate("cst|a| xor cst|b|"), is(false));
		assertThat(evaluate("cst|!a| xor cst|b|"), is(true));
		assertThat(evaluate("cst|!a| xor cst|!b|"), is(false));
	}
	
	@Test
	public void testAtomAlwaysFalseEval() {
		assertThat(evaluate("|toifk|"), is(false));
		assertThat(evaluate("|a|"), is(false));
		assertThat(evaluate("|to|"), is(false));
		assertThat(evaluate("fcr|toifk|"), is(false));
		assertThat(evaluate("totiir|toifk|"), is(false));
	}
	
	@Test(expected=SymbolMissingException.class)
	public void testTwoBlocksEvalSideEffectErrorDueToSameNameParseError() {
		//this eval declares a global frame
		Map<String, Boolean> frame = evaluateBlock("a = 1 b = 0 c = a ^ b");
		assertThat(lookup("a", frame), is(true));
		assertThat(lookup("b", frame), is(false));
		//the frame is still in the execution stack
		//this eval tries to change a with the value of b 
		frame = evaluateBlock("a = b b = !a");
		assertThat(lookup("a", frame), is(false));
		assertThat(lookup("b", frame), is(true));
	}
	
	@Test
	public void testTwoBlocksEvalSideEffect() {
		//this eval declares a global frame
		Map<String, Boolean> frame = evaluateBlock("a = 1 b = 0 c = a ^ b");
		assertThat(lookup("a", frame), is(true));
		//the frame is still in the execution stack
		//this eval changes a but b and c still present
		frame = evaluateBlock("a = 0");
		assertThat(lookup("a", frame), is(false));
		//previous value of b
		assertThat(lookup("b", frame), is(false));
		//previous value of c
		assertThat(lookup("c", frame), is(true));
	}
	
	@Test
	public void testBlock() {
		Map<String, Boolean> frame = evaluateBlock("a = 1 b = 1 c = a -> b");
		assertThat(lookup("a", frame), is(true));
		assertThat(lookup("b", frame), is(true));
		assertThat(lookup("c", frame), is(true));
	}
	@Test
	public void testLocalNotInGlobalFrame() {
		Map<String, Boolean> frame = evaluateBlock("a = 1 b = let x = 0, c = a -> x in c");
		assertThat(lookup("a", frame), is(true));
		assertThat(lookup("b", frame), is(false));
		assertNull(lookup("c", frame));
	}
	
	@Test
	public void testLetInLetInLet() {
		assertThat(evaluate("let a = 1 in let b = 1 in let c = a -> b in c"), is(true));
		assertThat(evaluate("let a = 1 in let b = 0 in let c = a -> b in c"), is(false));
		assertThat(evaluate("let a = 0 in let b = 1 in let c = a -> b in c"), is(true));
		assertThat(evaluate("let a = 0 in let b = 0 in let c = a -> b in c"), is(true));
	}
	
	@Test
	public void testLetInLet() {
		assertThat(evaluate("let a = 1, b = 1 in let c = a -> b in c"), is(true));
		assertThat(evaluate("let a = 1, b = 0 in let c = a -> b in c"), is(false));
		assertThat(evaluate("let a = 0, b = 1 in let c = a -> b in c"), is(true));
		assertThat(evaluate("let a = 0, b = 0 in let c = a -> b in c"), is(true));
	}
	
	@Test
	public void testImpLet() {
		assertThat(evaluate("let a = 1, b = 1, c = a -> b in c"), is(true));
		assertThat(evaluate("let a = 1, b = 0, c = a -> b in c"), is(false));
		assertThat(evaluate("let a = 0, b = 1, c = a -> b in c"), is(true));
		assertThat(evaluate("let a = 0, b = 0, c = a -> b in c"), is(true));
	}
	
	@Test
	public void testLetXOR() {
		assertThat(evaluate("let a = 1, b = 1 in a ^ b"), is(false));
		assertThat(evaluate("let a = 1, b = 0 in a ^ b"), is(true));
		assertThat(evaluate("let a = 0, b = 1 in a ^ b"), is(true));
		assertThat(evaluate("let a = 0, b = 0 in a ^ b"), is(false));
	}
	
	@Test
	public void testLetDisjunction() {
		assertThat(evaluate("let a = 1, b = 1 in a or b"), is(true));
		assertThat(evaluate("let a = 1, b = 0 in a or b"), is(true));
		assertThat(evaluate("let a = 0, b = 1 in a or b"), is(true));
		assertThat(evaluate("let a = 0, b = 0 in a or b"), is(false));
	}
	
	@Test
	public void testLetConjunction() {
		assertThat(evaluate("let a = 1, b = 1 in a & b"), is(true));
		assertThat(evaluate("let a = 1, b = 0 in a & b"), is(false));
		assertThat(evaluate("let a = 0, b = 1 in a & b"), is(false));
		assertThat(evaluate("let a = 0, b = 0 in a & b"), is(false));
	}
	
	@Test
	public void testLetNegation() {
		assertThat(evaluate("let a = 1 in !a"), is(false));
		assertThat(evaluate("let a = 0 in !a"), is(true));
	}
	
	@Test
	public void testEquivalence() {
		assertThat(evaluate("1 <=> 1"), is(true));
		assertThat(evaluate("1 <=> 0"), is(false));
		assertThat(evaluate("0 <=> 1"), is(false));
		assertThat(evaluate("0 <=> 0"), is(true));
	}
	
	@Test
	public void testImplication() {
		assertThat(evaluate("1 -> 1"), is(true));
		assertThat(evaluate("1 -> 0"), is(false));
		assertThat(evaluate("0 -> 1"), is(true));
		assertThat(evaluate("0 -> 0"), is(true));
	}
	
	@Test
	public void testXOR() {
		assertThat(evaluate("1 ^ 1"), is(false));
		assertThat(evaluate("1 ^ 0"), is(true));
		assertThat(evaluate("0 ^ 1"), is(true));
		assertThat(evaluate("0 ^ 0"), is(false));
	}
	
	@Test
	public void testDisjunction() {
		assertThat(evaluate("1 || 1"), is(true));
		assertThat(evaluate("1 || 0"), is(true));
		assertThat(evaluate("0 || 1"), is(true));
		assertThat(evaluate("0 || 0"), is(false));
	}
	
	@Test
	public void testConjunction() {
		assertThat(evaluate("true && true"), is(true));
		assertThat(evaluate("true && false"), is(false));
		assertThat(evaluate("false && true"), is(false));
		assertThat(evaluate("false && false"), is(false));
	}
	
	@Test
	public void testNegation() {
		assertThat(evaluate("!false"), is(true));
		assertThat(evaluate("!true"), is(false));
	}
	
	@Test
	public void testFalse() {
		assertThat(evaluate("false"), is(false));
	}
	
	@Test
	public void testTrue() {
		assertThat(evaluate("true"), is(true));
	}
	
	public Boolean lookup(String name, Map<String, Boolean> frame) {
		for (Map.Entry<String, Boolean> entry : frame.entrySet()) {
			if (entry.getKey().equals(name)) {
				return entry.getValue();
			}
		}
		return null;
	}
	
	public Map<String, Boolean> evaluateBlock(String code) {
		RuntimeException exception[] = new RuntimeException[1];
		DeclarationBlock block = Parser.parseBlock(code, e -> exception[0] = (RuntimeException)e);
		if (exception[0] != null) {
			throw exception[0];
		}
		return eval.evaluate(block);
	}
	
	public boolean evaluate(String code) {
		Expression exp = Parser.parse(code);
		return eval.evaluate(exp);
	}
	
	@Before
	public void setup() {
		eval = new Evaluator();
	}
}
