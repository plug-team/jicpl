package properties.PropositionalLogic.grammar;

import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.ParseTree;
import org.junit.Test;

import java.lang.reflect.Method;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class PropositionalLogicGrammarTest {
    private String currentRule;
    private Parser mParser;

    @Test
    public void testBlock() {
        setCurrentRule("block");
        assertParse(
                "form = \\  " +
                "   x       = fiacre| control.a && toto |, \n" +
                "   y       = cdl   | 23 |, \n" +
                "   lkfld   =       | jkjk | \n" +
                "in \n" +
                "    let z = x and y in\n" +
                "        true and false or  z -> |tot|\n" +
                "        \n" +
                "form = to and y");
    }

    @Test
    public void testOr() {
        setCurrentRule("formulaDeclaration");
        assertParse("a=|b||||c|");
    }

    @Test
    public void testLetFormula() {
    	setCurrentRule("formula");
    	assertParse("let x=|b| in x", "(formula let (formulaDeclarationList (formulaDeclaration x = (formula (atom |b|)))) in (formula x))");
    }
    
    @Test
    public void testComplexFormulaDeclaration() {
        setCurrentRule("formulaDeclaration");

        assertParse("aa = let x = |xfdf|, y = |to'm=z|, z = titi in true and false"
                , "(formulaDeclaration aa = " +
                    "(formula let " +
                        "(formulaDeclarationList " +
                            "(formulaDeclaration x = (formula (atom |xfdf|))) , " +
                            "(formulaDeclaration y = (formula (atom |to'm=z|))) , " +
                            "(formulaDeclaration z = (formula titi))) " +
                        "in (formula (formula (literal true)) and (formula (literal false)))))");
    }
    
    @Test
    public void testFormulaDeclaration(){
    	setCurrentRule("formulaDeclaration");
    	assertParse("a=b", "(formulaDeclaration a = (formula b))");
    	assertParse("a=true", "(formulaDeclaration a = (formula (literal true)))");
    	assertParse("a=|klgfkdlg|", "(formulaDeclaration a = (formula (atom |klgfkdlg|)))");
    	assertParse("a=not false", "(formulaDeclaration a = (formula not (formula (literal false))))");
    }
    
    @Test
    public void testRightAssociativity() {
    	setCurrentRule("formula");
    	assertParse("a -> b -> c", "(formula (formula a) -> (formula (formula b) -> (formula c)))");
    	assertParse("a <=> b <=> c", "(formula (formula a) <=> (formula (formula b) <=> (formula c)))");
    }
    
    @Test
    public void testParen() {
    	setCurrentRule("formula");
    	assertParse("a * (b * c)", "(formula (formula a) * (formula ( (formula (formula b) * (formula c)) )))");
    }
    
    @Test
    public void testLeftAssociativity() {
    	setCurrentRule("formula");
    	assertParse("a * b * c", "(formula (formula (formula a) * (formula b)) * (formula c))");
    	assertParse("a + b + c", "(formula (formula (formula a) + (formula b)) + (formula c))");
    	assertParse("a ^ b ^ c", "(formula (formula (formula a) ^ (formula b)) ^ (formula c))");
    }
    
    @Test
    public void testEquivalence() {
    	binaryExpressions("iff");
    	binaryExpressions("<->");
    	binaryExpressions("<=>");
    }
    
    @Test
    public void testImplication() {
    	binaryExpressions("implies");
    	binaryExpressions("->");
    	binaryExpressions("=>");
    }
    
    @Test
    public void testXor() {
    	binaryExpressions("xor");
    	binaryExpressions("^");
    }
    
    @Test
    public void testDisjunctionBar() {
    	setCurrentRule("formula");
    	assertParse("|xx|||bb", "(formula (formula (atom |xx|)) || (formula bb))");
    }
    
    @Test
    public void testDisjunction() {
    	binaryExpressions("or");
    	binaryExpressions("||");
    	binaryExpressions("\\/");
    	binaryExpressions("+");
    }
    
    @Test
    public void testConjunction() {
    	binaryExpressions("and");
    	binaryExpressions("&");
    	binaryExpressions("&&");
    	binaryExpressions("/\\");
    	binaryExpressions("*");
    }

    public void binaryExpressions(String operator) {
    	assertBinary("a", "a", operator, "bb", "bb");
    	assertBinary("|xx|", "(atom |xx|)", operator, "bb", "bb");
    	assertBinary("true", "(literal true)", operator, "bb", "bb");
    	assertBinary("~true", "~ (formula (literal true))", operator, "bb", "bb");
    }
    
    public void assertBinary(String lhs, String lhsExpected, String operator, String rhs, String rhsExpected) {
    	setCurrentRule("formula");
    	assertParse(lhs +" "+ operator +" "+ rhs, "(formula (formula "+lhsExpected+") " + operator + " (formula "+rhsExpected+"))");
    }
    
    @Test
    public void testNegation() {
    	setCurrentRule("formula");
    	assertParse("!a", "(formula ! (formula a))");
    	assertParse("not true", "(formula not (formula (literal true)))");
    	assertParse("~0", "(formula ~ (formula (literal 0)))");
    	assertParse("!|xxx|", "(formula ! (formula (atom |xxx|)))");
    	assertParse("!xy|true|", "(formula ! (formula (atom xy |true|)))");
    }
    
    @Test
    public void testFormulaAtom() {
    	setCurrentRule("formula");
    	assertParse("|ab|", "(formula (atom |ab|))");
    }
    
    @Test
    public void testAtom() {
    	setCurrentRule("atom");
    	assertParse("|a.b.c.d|", "(atom |a.b.c.d|)");
    	assertParse("|a|", "(atom |a|)");
    	assertParse("|a&&y==b|", "(atom |a&&y==b|)");
    	assertParse("|\\|b|", "(atom |\\|b|)");
    	assertParse("|a\\|b|", "(atom |a\\|b|)");
    	assertParse("|a\\||", "(atom |a\\||)");
    	assertParse("|\"b|", "(atom |\"b|)");
    	assertParse("|a\"b|", "(atom |a\"b|)");
    	assertParse("|a\"|", "(atom |a\"|)");
    	assertParse("lang|a==b|", "(atom lang |a==b|)" );
    	
    	assertParse("\"a\"", "(atom \"a\")");
    	assertParse("\"a&&y==b\"", "(atom \"a&&y==b\")");
    	assertParse("\"\\|b\"", "(atom \"\\|b\")");
    	assertParse("\"a\\|b\"", "(atom \"a\\|b\")");
    	assertParse("\"a\\|\"", "(atom \"a\\|\")");
    	assertParse("\"\"b\"", "(atom \"\"b\")");
    	assertParse("\"a\"b\"", "(atom \"a\"b\")");
    	assertParse("\"a\"\"", "(atom \"a\"\")");
    	assertParse("lang\"a==b\"", "(atom lang \"a==b\")" );
    }
    
    @Test
    public void testIdentifier() {
    	setCurrentRule("formula");
    	assertParse("a", "(formula a)");
    	assertParse("abcd", "(formula abcd)");
    	assertParse("x99", "(formula x99)");
    	
    }
    
    @Test
    public void testFormulaLiteral() {
    	setCurrentRule("formula");
    	assertParse("true", "(formula (literal true))");
    	assertParse("1", "(formula (literal 1))");
    	assertParse("false", "(formula (literal false))");
    	assertParse("0", "(formula (literal 0))");
    }
    
    @Test
    public void testLiterals() {
    	setCurrentRule("literal");
    	assertParse("true", "(literal true)");
    	assertParse("1", "(literal 1)");
    	assertParse("false", "(literal false)");
    	assertParse("0", "(literal 0)");
    }
    
    @Test
    public void testReservedConjunction() {
    	setCurrentRule("reserved");
    	assertParse("and", "(reserved and)");
    	assertParse("&", "(reserved &)");
    	assertParse("&&", "(reserved &&)");
        assertParse("/\\", "(reserved /\\)");
        assertParse("*", "(reserved *)");
    }
    
    @Test
    public void testReservedDisjunction() {
    	setCurrentRule("reserved");
    	assertParse("or", "(reserved or)");
    	assertParse("||", "(reserved ||)");
        assertParse("\\/", "(reserved \\/)");
        assertParse("+", "(reserved +)");
    }
    
    @Test
    public void testReservedEquivalence() {
    	setCurrentRule("reserved");
    	assertParse("iff", "(reserved iff)");
    	assertParse("<->", "(reserved <->)");
    	assertParse("<=>", "(reserved <=>)");
    }
    
    @Test
    public void testReservedFalse() {
    	setCurrentRule("reserved");
    	assertParse("false", "(reserved false)");
        assertParse("0", "(reserved 0)");
    }
    
    @Test
    public void testReservedImplication() {
    	setCurrentRule("reserved");
    	assertParse("implies", "(reserved implies)");
    	assertParse("->", "(reserved ->)");
    	assertParse("=>", "(reserved =>)");
    }
    
    @Test
    public void testReservedIn() {
    	setCurrentRule("reserved");
    	assertParse("in", "(reserved in)");
    }
    
    @Test
    public void testReservedLet() {
    	setCurrentRule("reserved");
    	assertParse("let", "(reserved let)");
    }
    
    @Test
    public void testReservedNegation() {
    	setCurrentRule("reserved");
        assertParse("!", "(reserved !)");
        assertParse("~", "(reserved ~)");
        assertParse("not", "(reserved not)");
    }
    
    @Test
    public void testReservedXor() {
    	setCurrentRule("reserved");
        assertParse("xor", "(reserved xor)");
        assertParse("^", "(reserved ^)");
    }
    
    @Test
    public void testReservedTrue() {
    	setCurrentRule("reserved");
    	assertParse("true", "(reserved true)");
        assertParse("1", "(reserved 1)");
    }

    public ParseTree assertParse(String input, String expectation) {
        ParseTree result = parse(input, currentRule);
        assertTrue(result != null);

        Object actual = result.toStringTree(mParser);
        assertEquals("testing " + input + " gives " + expectation, expectation, actual);
        return result;
    }

    public ParseTree parse(CharStream cs, String rule) {
        PropositionalLogicLexer lexer 	= new PropositionalLogicLexer(cs);
        CommonTokenStream       tokens  = new CommonTokenStream(lexer);
        Parser                  parser  = new PropositionalLogicParser(tokens);
        NoErrorsForTest  	    errorL  = new NoErrorsForTest();

        mParser = parser;
        //parser.removeErrorListeners();
        parser.addErrorListener(errorL);
        try {
            Method mtd = parser.getClass().getMethod(rule);
            ParseTree pt = (ParseTree)mtd.invoke(parser);
            if (errorL.hasErrors()) return null;
            return pt;
        } catch (Exception e) {
            System.err.println("no matching method for rule: " + rule);
            return null;
        }
    }
    public static class NoErrorsForTest extends BaseErrorListener {
        private Boolean hasErrors = false;
        @Override
        public void syntaxError(Recognizer<?, ?> rec, Object offendingSymbol, int line, int column, String msg, RecognitionException e) {
            hasErrors = true;
        }
        public Boolean hasErrors() {
            return hasErrors;
        }
    }

    public void setCurrentRule(String currentRule) {
        this.currentRule = currentRule;
    }
    public ParseTree parse(String input, String rule) {
        ANTLRInputStream 	is		= new ANTLRInputStream(input);
        return parse(is, rule);
    }
    public ParseTree assertParse(String input) {
        ParseTree result = parse(input, currentRule);
        assertTrue(result != null);
        return result;
    }

}
