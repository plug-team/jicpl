package properties.PropositionalLogic.interpreter;

import properties.PropositionalLogic.PropositionalLogicModel.*;
import properties.PropositionalLogic.PropositionalLogicModel.util.PropositionalLogicModelSwitch;

import java.lang.Boolean;
import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

public class EvaluatorStatic extends PropositionalLogicModelSwitch<Boolean> {
    //execution stack, composed of frames, each frame associates a boolean value to a ExpressionDeclaration
    Stack<Map<String, Boolean>> executionStack = new Stack<>();

    public Map<String, Boolean> globalFrame() {
        if (executionStack.isEmpty()) return null;
        return executionStack.get(0);
    }

    Map<String, Boolean> pushFrame() {
        return executionStack.push(new HashMap<>());
    }

    Map<String, Boolean> popFrame() {
        return executionStack.pop();
    }

    Map<String, Boolean> currentFrame() {
        return executionStack.peek();
    }

    Boolean lookup(Declaration declaration) {
        for (int i=executionStack.size()-1; i>=0; i--) {
            Map<String, Boolean> frame = executionStack.get(i);
            Boolean value = frame.get(declaration.getName());
            if (value != null) {
                return value;
            }
        }
        throw new RuntimeException("The declaration '" + declaration.getName() + "' was not found on the execution stack");
    }

    @Override
    public Boolean caseTrue(True object) {
        return true;
    }

    @Override
    public Boolean caseFalse(False object) {
        return false;
    }

    @Override
    public Boolean caseAtom(Atom object) {
        throw new UnsupportedOperationException("Atom evaluation in static context");
    }

    @Override
    public Boolean caseExpressionReference(ExpressionReference object) {
        //the evaluation of the expression is done at the declaration site
        return lookup(object.getExp());
    }

    @Override
    public Boolean caseLogicalNegation(LogicalNegation object) {
        return ! doSwitch(object.getOperand());
    }

    @Override
    public Boolean caseLogicalConjunction(LogicalConjunction object) {
        Boolean lhs = doSwitch(object.getLhs());
        Boolean rhs = doSwitch(object.getRhs());
        return lhs && rhs;
    }

    @Override
    public Boolean caseLogicalDisjunction(LogicalDisjunction object) {
        Boolean lhs = doSwitch(object.getLhs());
        Boolean rhs = doSwitch(object.getRhs());
        return lhs || rhs;
    }

    @Override
    public Boolean caseLogicalXOR(LogicalXOR object) {
        Boolean lhs = doSwitch(object.getLhs());
        Boolean rhs = doSwitch(object.getRhs());
        return lhs ^ rhs;
    }

    @Override
    public Boolean caseLogicalImplication(LogicalImplication object) {
        Boolean lhs = doSwitch(object.getLhs());
        Boolean rhs = doSwitch(object.getRhs());
        return (!lhs) || rhs;
    }

    @Override
    public Boolean caseLogicalEquivalence(LogicalEquivalence object) {
        Boolean lhs = doSwitch(object.getLhs());
        Boolean rhs = doSwitch(object.getRhs());
        return ! (lhs ^ rhs);
    }

    @Override
    public Boolean caseExpressionDeclaration(ExpressionDeclaration object) {
        Boolean value = doSwitch(object.getExpression());
        if (currentFrame().containsKey(object)) {
            throw new RuntimeException("strangely there is already a value for '" + object.getName() + "' in the currentFrame");
        }
        currentFrame().put(object.getName(), value);
        return value;
    }

    @Override
    public Boolean caseLetExpression(LetExpression object) {
        pushFrame();
        //evaluate the local variables
        for (Declaration decl : object.getDeclarations()) {
            doSwitch(decl);
        }
        //evaluate the expression in the currentFrame context
        Boolean result = doSwitch(object.getExpression());
        popFrame();
        return result;
    }

    @Override
    public Boolean caseDeclarationBlock(DeclarationBlock object) {
        //evaluates the declarations in order and puts the results in the enclosing frame
        //if no global frame is present one is pushed but never popped, it stores the results of the evaluations
        if (executionStack.isEmpty()) {
            pushFrame();
        }
        Boolean value = false;
        for (Declaration decl : object.getDeclarations()) {
            value = doSwitch(decl);
        }

        //returns the value of the last declaration, defaults to false
        return value;
    }

    public boolean evaluate(Expression exp) {
        return doSwitch(exp);
    }

    public Map<String, Boolean> evaluate(DeclarationBlock block) {
        doSwitch(block);
        return globalFrame();
    }
}
