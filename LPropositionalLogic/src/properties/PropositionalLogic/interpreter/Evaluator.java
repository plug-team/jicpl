package properties.PropositionalLogic.interpreter;

import properties.PropositionalLogic.PropositionalLogicModel.Atom;

public class Evaluator extends EvaluatorStatic {
	//for the evaluation of atoms, this should be set from outside
	AtomEvaluatorDispatcher atomEvaluator = new AtomEvaluatorDispatcher();
	
	public void setDefaultEvaluator(IAtomEvaluator evaluator) {
		atomEvaluator.setDefaultEvaluator(evaluator);
	}
	
	public void addAtomEvaluator(String language, IAtomEvaluator evaluator) {
		atomEvaluator.addEvaluator(language, evaluator);
	}

	@Override
	public Boolean caseAtom(Atom object) {
		return atomEvaluator.evaluate(object);
	}
}
