package properties.PropositionalLogic.interpreter.atom;

import properties.PropositionalLogic.interpreter.IAtomEvaluator;

public class AlwaysFalseAtomEvaluator implements IAtomEvaluator {

	@Override
	public Object parse(String code) {
		return null;
	}

	@Override
	public boolean evaluate(Object atomicBooleanExpression) {
		return false;
	}

}
