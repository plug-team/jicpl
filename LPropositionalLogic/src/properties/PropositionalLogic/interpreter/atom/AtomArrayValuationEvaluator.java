package properties.PropositionalLogic.interpreter.atom;

import properties.PropositionalLogic.interpreter.IAtomEvaluator;
import properties.PropositionalLogic.parser.SymbolResolver;

import java.util.Map;

public class AtomArrayValuationEvaluator implements IAtomEvaluator {
    public Map<String, Integer> atomicPropositions;
    boolean valuation[];

    public AtomArrayValuationEvaluator(Map<String, Integer> atomicPropositions) {
        this.atomicPropositions = atomicPropositions;
    }

    public void setValuation(boolean valuation[]) {
        this.valuation = valuation;
    }

    @Override
    public Object parse(String code) {
        Integer value = atomicPropositions.get(code);
        if (value == null) {
            throw new SymbolResolver.SymbolMissingException("'" + code + "' is not present in the constant map");
        }
        return value;
    }

    @Override
    public boolean evaluate(Object atomicBooleanExpression) {
        return valuation[(int)atomicBooleanExpression];
    }
}
