package properties.PropositionalLogic.interpreter.atom;

import properties.PropositionalLogic.interpreter.IAtomEvaluator;
import properties.PropositionalLogic.parser.SymbolResolver.SymbolMissingException;

import java.util.Map;

public class AtomConstantMapEvaluator implements IAtomEvaluator {
	Map<String, Boolean> constantMap;
	
	public AtomConstantMapEvaluator(Map<String, Boolean> constantMap) {
		this.constantMap = constantMap;
	}

	@Override
	public Object parse(String code) {
		Boolean value = constantMap.get(code);
		if (value == null) {
			throw new SymbolMissingException("'" + code + "' is not present in the constant map");
		}
		return code;
	}

	@Override
	public boolean evaluate(Object atomicBooleanExpression) {
		Boolean value = constantMap.get((String)atomicBooleanExpression);
		if (value == null) {
			throw new SymbolMissingException("'" + (String)atomicBooleanExpression + "' is not present in the constant map");
		}
		return value;
	}
}
