package properties.PropositionalLogic.interpreter;

import properties.PropositionalLogic.PropositionalLogicModel.*;
import properties.PropositionalLogic.PropositionalLogicModel.util.PropositionalLogicModelSwitch;

import java.lang.Boolean;
import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

public class EvaluatorLazyStatic extends PropositionalLogicModelSwitch<Boolean> {
    //execution stack, composed of frames, each frame associates a boolean value to a ExpressionDeclaration
    Stack<Map<String, Expression>> executionStack = new Stack<>();

    public Map<String, Expression> globalFrame() {
        if (executionStack.isEmpty()) return null;
        return executionStack.get(0);
    }

    Map<String, Expression> pushFrame() {
        return executionStack.push(new HashMap<>());
    }

    Map<String, Expression> popFrame() {
        return executionStack.pop();
    }

    Map<String, Expression> currentFrame() {
        return executionStack.peek();
    }

    Expression lookup(Declaration declaration) {
        for (int i=executionStack.size()-1; i>=0; i--) {
            Map<String, Expression> frame = executionStack.get(i);
            Expression value = frame.get(declaration.getName());
            if (value != null) {
                return value;
            }
        }
        throw new RuntimeException("The declaration '" + declaration.getName() + "' was not found on the execution stack");
    }

    @Override
    public Boolean caseTrue(True object) {
        return true;
    }

    @Override
    public Boolean caseFalse(False object) {
        return false;
    }

    @Override
    public Boolean caseAtom(Atom object) {
        throw new UnsupportedOperationException("Atom evaluation in static context");
    }

    @Override
    public Boolean caseExpressionReference(ExpressionReference object) {
        //the evaluation is done when an expression reference is found
        Expression exp = lookup(object.getExp());
        return evaluate(exp);
    }

    @Override
    public Boolean caseLogicalNegation(LogicalNegation object) {
        return ! doSwitch(object.getOperand());
    }

    @Override
    public Boolean caseLogicalConjunction(LogicalConjunction object) {
        Boolean lhs = doSwitch(object.getLhs());
        Boolean rhs = doSwitch(object.getRhs());
        return lhs && rhs;
    }

    @Override
    public Boolean caseLogicalDisjunction(LogicalDisjunction object) {
        Boolean lhs = doSwitch(object.getLhs());
        Boolean rhs = doSwitch(object.getRhs());
        return lhs || rhs;
    }

    @Override
    public Boolean caseLogicalXOR(LogicalXOR object) {
        Boolean lhs = doSwitch(object.getLhs());
        Boolean rhs = doSwitch(object.getRhs());
        return lhs ^ rhs;
    }

    @Override
    public Boolean caseLogicalImplication(LogicalImplication object) {
        Boolean lhs = doSwitch(object.getLhs());
        Boolean rhs = doSwitch(object.getRhs());
        return (!lhs) || rhs;
    }

    @Override
    public Boolean caseLogicalEquivalence(LogicalEquivalence object) {
        Boolean lhs = doSwitch(object.getLhs());
        Boolean rhs = doSwitch(object.getRhs());
        return ! (lhs ^ rhs);
    }

    @Override
    public Boolean caseExpressionDeclaration(ExpressionDeclaration object) {
        //on expression declaration put the expression in frame
        if (currentFrame().containsKey(object)) {
            throw new RuntimeException("strangely there is already a value for '" + object.getName() + "' in the currentFrame");
        }
        currentFrame().put(object.getName(), object.getExpression());
        //no evaluation is done at the declaration site
        return false;
    }

    @Override
    public Boolean caseLetExpression(LetExpression object) {
        pushFrame();
        //evaluate the local variables
        for (Declaration decl : object.getDeclarations()) {
            doSwitch(decl);
        }
        //evaluate the expression in the currentFrame context
        Boolean result = doSwitch(object.getExpression());
        popFrame();
        return result;
    }

    @Override
    public Boolean caseDeclarationBlock(DeclarationBlock object) {
        //evaluates the declarations in order and puts the results in the enclosing frame
        //if no global frame is present one is pushed but never popped, it stores the results of the evaluations
        if (executionStack.isEmpty()) {
            pushFrame();
        }
        for (Declaration decl : object.getDeclarations()) {
            doSwitch(decl);
        }
        //no evaluation is done at the declaration site
        return false;
    }

    public boolean evaluate(Expression exp) {
        return doSwitch(exp);
    }

    public Map<String, Expression> evaluate(DeclarationBlock block) {
        doSwitch(block);
        return globalFrame();
    }
}
