package properties.PropositionalLogic.interpreter;

import properties.PropositionalLogic.PropositionalLogicModel.*;
import properties.PropositionalLogic.PropositionalLogicModel.util.PropositionalLogicModelSwitch;

import java.lang.Boolean;

public class EvaluatorSpaghetti extends PropositionalLogicModelSwitch<Boolean> {

    //for the evaluation of atoms, this should be set from outside
    AtomEvaluatorDispatcher atomEvaluator = new AtomEvaluatorDispatcher();

    public void setDefaultEvaluator(IAtomEvaluator evaluator) {
        atomEvaluator.setDefaultEvaluator(evaluator);
    }

    public void addAtomEvaluator(String language, IAtomEvaluator evaluator) {
        atomEvaluator.addEvaluator(language, evaluator);
    }

    @Override
    public Boolean caseAtom(Atom object) {
        return atomEvaluator.evaluate(object);
    }

    @Override
    public Boolean caseTrue(True object) {
        return true;
    }

    @Override
    public Boolean caseFalse(False object) {
        return false;
    }

    @Override
    public Boolean caseExpressionReference(ExpressionReference object) {
        return doSwitch(object.getExp());
    }

    @Override
    public Boolean caseLogicalNegation(LogicalNegation object) {
        return ! doSwitch(object.getOperand());
    }

    @Override
    public Boolean caseLogicalConjunction(LogicalConjunction object) {
        Boolean lhs = doSwitch(object.getLhs());
        Boolean rhs = doSwitch(object.getRhs());
        return lhs && rhs;
    }

    @Override
    public Boolean caseLogicalDisjunction(LogicalDisjunction object) {
        Boolean lhs = doSwitch(object.getLhs());
        Boolean rhs = doSwitch(object.getRhs());
        return lhs || rhs;
    }

    @Override
    public Boolean caseLogicalXOR(LogicalXOR object) {
        Boolean lhs = doSwitch(object.getLhs());
        Boolean rhs = doSwitch(object.getRhs());
        return lhs ^ rhs;
    }

    @Override
    public Boolean caseLogicalImplication(LogicalImplication object) {
        Boolean lhs = doSwitch(object.getLhs());
        Boolean rhs = doSwitch(object.getRhs());
        return (!lhs) || rhs;
    }

    @Override
    public Boolean caseLogicalEquivalence(LogicalEquivalence object) {
        Boolean lhs = doSwitch(object.getLhs());
        Boolean rhs = doSwitch(object.getRhs());
        return ! (lhs ^ rhs);
    }

    @Override
    public Boolean caseExpressionDeclaration(ExpressionDeclaration object) {
        return doSwitch(object.getExpression());
    }

    @Override
    public Boolean caseLetExpression(LetExpression object) {
        return doSwitch(object.getExpression());
    }

    @Override
    public Boolean caseDeclarationBlock(DeclarationBlock object) {
        throw new RuntimeException("This evaluator can only evaluate expressions");
    }

    public boolean evaluate(Expression exp) {
        return doSwitch(exp);
    }
}
