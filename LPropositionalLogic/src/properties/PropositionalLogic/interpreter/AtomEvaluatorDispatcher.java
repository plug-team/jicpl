package properties.PropositionalLogic.interpreter;

import properties.PropositionalLogic.PropositionalLogicModel.Atom;
import properties.PropositionalLogic.interpreter.atom.AlwaysFalseAtomEvaluator;

import java.util.HashMap;
import java.util.Map;

public class AtomEvaluatorDispatcher {
	Map<String, CompiledAtom> atomCache = new HashMap<>();
	
	Map<String, IAtomEvaluator> evaluatorMap = new HashMap<String, IAtomEvaluator>();
	IAtomEvaluator defaultEvaluator = new AlwaysFalseAtomEvaluator();
	
	public void addEvaluator(String language, IAtomEvaluator evaluator) {
		evaluatorMap.put(language, evaluator);
	}
	
	public void setDefaultEvaluator(IAtomEvaluator evaluator) {
		this.defaultEvaluator = evaluator;
	}
	
	public boolean evaluate(Atom atom) {
		String key = cacheKey(atom);
		CompiledAtom compiledAtom = atomCache.get(key);
		if (compiledAtom == null) {
			compiledAtom = compile(atom);
			atomCache.put(key, compiledAtom);
		}

		return compiledAtom.evaluator.evaluate(compiledAtom.atomModel);
	}
	
	private CompiledAtom compile(Atom atom) {
		IAtomEvaluator concreteEvaluator = evaluatorMap.get(atom.getLanguage());
		if (concreteEvaluator == null) {
			concreteEvaluator = defaultEvaluator;
		}
		CompiledAtom cA = new CompiledAtom();
		cA.atomModel = concreteEvaluator.parse(atom.getCode());
		cA.evaluator = concreteEvaluator;
		return cA;
	}
	
	private String cacheKey(Atom atom) {
		return atom.getLanguage() +"|"+ atom.getCode();
	}
	
	private class CompiledAtom {
		public IAtomEvaluator evaluator;
		public Object atomModel;
	}
}
