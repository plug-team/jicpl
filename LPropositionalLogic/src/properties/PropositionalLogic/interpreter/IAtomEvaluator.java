package properties.PropositionalLogic.interpreter;

public interface IAtomEvaluator {
	//this method should return an object representing the compiled version of the atom
	Object parse(String code);
	//this method actually evaluates the atom
	//TODO: do we need here a way to specify the evaluation context?
	boolean evaluate(Object atomicBooleanExpression);
}
