package properties.PropositionalLogic.transformations;

import properties.PropositionalLogic.PropositionalLogicModel.*;
import properties.PropositionalLogic.PropositionalLogicModel.util.PropositionalLogicModelSwitch;

import java.util.Stack;

public class PrettyPrinter extends PropositionalLogicModelSwitch<String> {
	
	public static String print(Element e) {
		PrettyPrinter pp = new PrettyPrinter();
		return pp.doSwitch(e);
	}
	
	@Override
	public String caseTrue(True object) {
		return "true";
	}
	
	@Override
	public String caseFalse(False object) {
		return "false";
	}
	
	@Override
	public String caseAtom(Atom object) {
		String delim = object.getDelimiterString();
		if (delim == null) {
			if (object.getCode().indexOf('|')==-1) {
				delim = "|";
			}
			else if (object.getCode().indexOf('"')==-1) {
				delim = "\"";
			}
		}
		return (object.getLanguage() == null ? "" : object.getLanguage()) + delim + object.getCode() + delim;
	}
	
	@Override
	public String caseExpressionReference(ExpressionReference object) {
		return object.getExp().getName();
	}
	
	@Override
	public String caseLogicalNegation(LogicalNegation object) {
		return object.getOperatorToken() + " " + doSwitch(object.getOperand());
	}
	
	@Override
	public String caseBinaryExpression(BinaryExpression object) {
		String lhs = doSwitch(object.getLhs());
		String rhs = doSwitch(object.getRhs());
		return "(" + lhs +" "+ object.getOperatorToken() +" "+ rhs + ")";
	}
	
	@Override
	public String caseExpressionDeclaration(ExpressionDeclaration object) {
		return object.getName() + " = " + doSwitch(object.getExpression());
	}
	
	@Override
	public String caseDeclarationBlock(DeclarationBlock object) {
		String result = "";
		for (Declaration eD : object.getDeclarations()) {
			result += doSwitch(eD) + "\n";
		}
		return result;
	}

	Stack<Integer> tabs = new Stack<Integer>();
	private void pushTab() {
		tabs.push(0);
	}
	private void popTab() {
		tabs.pop();
	}
	private void inctab() {
		tabs.set(tabs.size()-1, tabs.peek() + 1);
	}
	private void dectab() {
		tabs.set(tabs.size()-1, tabs.peek() - 1);
	}
	
	private String tabs() {
		String s = "";
		for (int j = 0; j<tabs.size(); j++) {
			int tab = tabs.get(j);
			for (int i = 0; i<tab; i++) {
				s += "\t";
			}
		}
		return s;
	}
	@Override
	public String caseLetExpression(LetExpression object) {
		String result = "let\n";
		pushTab();
		inctab();
		for (Declaration eD : object.getDeclarations()) {
			result += tabs() + doSwitch(eD) + "\n";
		}
		dectab();
		result += tabs()+"in\n";
		inctab();
		result += tabs() + doSwitch(object.getExpression());
		popTab();
		return result;
	}
}
