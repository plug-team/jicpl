package properties.PropositionalLogic.transformations;

import properties.PropositionalLogic.PropositionalLogicModel.*;
import properties.PropositionalLogic.PropositionalLogicModel.util.PropositionalLogicModelSwitch;

import java.util.HashMap;
import java.util.IdentityHashMap;
import java.util.Map;
import java.util.Stack;

public class PropositionalLogic2AbstractText extends PropositionalLogicModelSwitch<String>  {
    @Override
    public String caseFalse(False object) {
        return "false";
    }
    @Override
    public String caseTrue(True object) {
        return "true";
    }
    @Override
    public String caseLogicalNegation(LogicalNegation object) {
        String operand = doSwitch(object.getOperand());
        return "(! " + operand + ")";
    }
    @Override
    public String caseLogicalImplication(LogicalImplication object) {
        String lhs = doSwitch(object.getLhs());
        String rhs = doSwitch(object.getRhs());
        return "(" + lhs + " -> " + rhs + ")";
    }
    @Override
    public String caseLogicalEquivalence(LogicalEquivalence object) {
        String lhs = doSwitch(object.getLhs());
        String rhs = doSwitch(object.getRhs());
        return "(" + lhs + " <-> " + rhs + ")";
    }
    @Override
    public String caseLogicalConjunction(LogicalConjunction object) {
        String lhs = doSwitch(object.getLhs());
        String rhs = doSwitch(object.getRhs());
        return "(" + lhs + " && " + rhs + ")";
    }
    @Override
    public String caseLogicalDisjunction(LogicalDisjunction object) {
        String lhs = doSwitch(object.getLhs());
        String rhs = doSwitch(object.getRhs());
        return "(" + lhs + " || " + rhs + ")";
    }

    @Override
    public String caseLogicalXOR(LogicalXOR object) {
        String lhs = doSwitch(object.getLhs());
        String rhs = doSwitch(object.getRhs());
        return "(" + lhs + " xor " + rhs + ")";
    }

    public Map<String, Atom> name2atom;
    Map<Atom, String> atom2name = new IdentityHashMap<>();
    @Override
    public String caseAtom(Atom object) {
        String name = atom2name.get(object);
        if (name != null) return name;
        name = newAtomName();
        atom2name.put(object, name);
        name2atom.put(name, object);
        return name;
    }
    public int idx = 0;
    private String newAtomName() {
        return "a" + idx++;
    }

    @Override
    public String caseExpressionReference(ExpressionReference object) {
        ExpressionDeclaration decl = object.getExp();
        return doSwitch(decl.getExpression());
    }

    String lookup(String name) {
        for (int i=context.size()-1; i>=0; i--) {
            String value = context.get(i).get(name);
            if (value != null) {
                return value;
            }
        }
        return null;
    }
    Stack<Map<String,String>> context = new Stack<>();
    @Override
    public String caseLetExpression(LetExpression object) {
        Map<String, String> frame = new HashMap<>();
        for (Declaration decl : object.getDeclarations()) {
            String name = decl.getName();
            String formula = doSwitch(((ExpressionDeclaration)decl).getExpression());
            frame.put(name, formula);
        }
        context.push(frame);
        return doSwitch(object.getExpression());
    }

    @Override
    public String caseExpressionDeclaration(ExpressionDeclaration object) {
        String value = lookup(object.getName());
        if (value != null) return value;
        value = doSwitch(object.getExpression());
        return value;
    }
}
