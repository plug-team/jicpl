package properties.PropositionalLogic.transformations;

import org.eclipse.emf.ecore.EObject;
import properties.PropositionalLogic.PropositionalLogicModel.*;
import properties.PropositionalLogic.PropositionalLogicModel.util.PropositionalLogicModelSwitch;

import java.lang.Boolean;
import java.util.*;

public class AtomExtractor {

    public List<Atom> getAtoms(Declaration declaration, Set<EObject> known) {
        Extractor extractor = new Extractor(known);
        extractor.doSwitch(declaration);
        return extractor.atoms;
    }

    public List<Atom> getAtoms(Expression expression, Set<EObject> known) {
        Extractor extractor = new Extractor(known);
        extractor.doSwitch(expression);
        return extractor.atoms;
    }

    class Extractor extends PropositionalLogicModelSwitch<Boolean> {
        Set<EObject> known;
        List<Atom> atoms = new ArrayList<>();

        public Extractor(Set<EObject> known) {
            this.known = known;
            if (this.known == null) {
                this.known = Collections.newSetFromMap(new IdentityHashMap<>());
            }
        }

        @Override
        public Boolean caseAtom(Atom object) {
            if (known.contains(object)) {
                return true;
            }
            atoms.add(object);
            known.add(object);
            return true;
        }

        @Override
        public Boolean caseLogicalNegation(LogicalNegation object) {
            if (known.contains(object)) {
                return true;
            }
            doSwitch(object.getOperand());
            known.add(object);
            return true;
        }

        @Override
        public Boolean caseUnaryExpression(UnaryExpression object) {
            if (known.contains(object)) {
                return true;
            }
            known.add(object);
            return doSwitch(object.getOperand());
        }

        @Override
        public Boolean caseBinaryExpression(BinaryExpression object) {
            if (known.contains(object)) {
                return true;
            }
            doSwitch(object.getLhs());
            doSwitch(object.getRhs());
            known.add(object);
            return true;
        }

        @Override
        public Boolean caseExpressionReference(ExpressionReference object) {
            if (known.contains(object)) {
                return true;
            }
            doSwitch(object.getExp());
            known.add(object);
            return true;
        }

        @Override
        public Boolean caseExpressionDeclaration(ExpressionDeclaration object) {
            if (known.contains(object)) {
                return true;
            }
            doSwitch(object.getExpression());
            known.add(object);
            return true;
        }

        @Override
        public Boolean caseFalse(False object) {
            return true;
        }

        @Override
        public Boolean caseTrue(True object) {
            return true;
        }

        @Override
        public Boolean caseLetExpression(LetExpression object) {
            if (known.contains(object)) {
                return true;
            }
            for (Declaration decl : object.getDeclarations()) {
                doSwitch(decl);
            }
            doSwitch(object.getExpression());
            known.add(object);
            return true;
        }

        @Override
        public Boolean caseDeclarationBlock(DeclarationBlock object) {
            if (known.contains(object)) {
                return true;
            }
            for (Declaration decl : object.getDeclarations()) {
                doSwitch(decl);
            }
            known.add(object);
            return true;
        }
    }
}
