package properties.PropositionalLogic.transformations;

import properties.PropositionalLogic.PropositionalLogicModel.Element;
import properties.PropositionalLogic.PropositionalLogicModel.ExpressionReference;
import properties.PropositionalLogic.PropositionalLogicModel.LetExpression;

public class PrettyPrinterInline extends PrettyPrinter {

    public static String print(Element e) {
        PrettyPrinterInline pp = new PrettyPrinterInline();
        return pp.doSwitch(e);
    }

    @Override
    public String caseExpressionReference(ExpressionReference object) {
        return doSwitch(object.getExp().getExpression());
    }
    @Override
    public String caseLetExpression(LetExpression object) {
        return doSwitch(object.getExpression());
    }
}
