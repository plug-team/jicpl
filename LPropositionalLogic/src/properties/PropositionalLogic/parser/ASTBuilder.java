package properties.PropositionalLogic.parser;

import org.antlr.v4.runtime.tree.ParseTreeProperty;
import properties.PropositionalLogic.PropositionalLogicModel.*;
import properties.PropositionalLogic.grammar.PropositionalLogicBaseListener;
import properties.PropositionalLogic.grammar.PropositionalLogicParser;
import properties.PropositionalLogic.grammar.PropositionalLogicParser.*;

import java.util.ArrayList;
import java.util.List;

public class ASTBuilder extends PropositionalLogicBaseListener implements TreeAnnotator {
	public ParseTreeProperty<Object> values = new ParseTreeProperty<>();
	public ParseTreeProperty<Object> getValues() {
		return values;
	};
	
    PropositionalLogicModelFactory factory = PropositionalLogicModelFactory.eINSTANCE;
    
    @Override
    public void exitLiteral(LiteralContext ctx) {    
    	//ideally true and false should be singletons, but there are containment problems
        setValue(ctx, ctx.TRUE() == null ? factory.createFalse() : factory.createTrue());
    }
    
    @Override
    public void exitAtom(AtomContext ctx) {
    	Atom atom = factory.createAtom();
    	String language = ctx.IDENTIFIER() != null ? ctx.IDENTIFIER().getText() : null;
    	atom.setLanguage(language);
    	String code = ctx.ATOMINLINE().getText();
    	//remove the '|' or '"' at the beginning and at the end
    	atom.setCode(code.substring(1, code.length()-1));
    	
    	atom.setDelimiterString(code.substring(0,1));
    	setValue(ctx, atom);
    }
    
    @Override
    public void exitParenExp(ParenExpContext ctx) {
    	setValue(ctx, getValue(ctx.formula()));
    }
    
    @Override
    public void exitUnaryExp(UnaryExpContext ctx) {
    	LogicalNegation value = factory.createLogicalNegation();
    	value.setOperatorToken(ctx.NEGATION().getText());
    	value.setOperand(getValue(ctx.formula(), Expression.class));
    	setValue(ctx, value);
    }
    
    @Override
    public void exitBinaryExp(BinaryExpContext ctx) {
    	switch (ctx.operator.getType()) {
			case PropositionalLogicParser.CONJUNCTION: {
				LogicalConjunction value = factory.createLogicalConjunction();
				value.setOperatorToken(ctx.operator.getText());
				value.setLhs(getValue(ctx.formula(0), Expression.class));
				value.setRhs(getValue(ctx.formula(1), Expression.class));
				setValue(ctx, value);
    			return;
			}
			case PropositionalLogicParser.DISJUNCTION: {
				LogicalDisjunction value = factory.createLogicalDisjunction();
				value.setOperatorToken(ctx.operator.getText());
				value.setLhs(getValue(ctx.formula(0), Expression.class));
				value.setRhs(getValue(ctx.formula(1), Expression.class));
				setValue(ctx, value);
				return;
			}
			case PropositionalLogicParser.XOR: {
				LogicalXOR value = factory.createLogicalXOR();
				value.setOperatorToken(ctx.operator.getText());
				value.setLhs(getValue(ctx.formula(0), Expression.class));
				value.setRhs(getValue(ctx.formula(1), Expression.class));
				setValue(ctx, value);
				return;
			}
			case PropositionalLogicParser.IMPLICATION: {
				LogicalImplication value = factory.createLogicalImplication();
				value.setOperatorToken(ctx.operator.getText());
				value.setLhs(getValue(ctx.formula(0), Expression.class));
				value.setRhs(getValue(ctx.formula(1), Expression.class));
				setValue(ctx, value);
				return;
			}
			case PropositionalLogicParser.EQUIVALENCE: {
				LogicalEquivalence value = factory.createLogicalEquivalence();
				value.setOperatorToken(ctx.operator.getText());
				value.setLhs(getValue(ctx.formula(0), Expression.class));
				value.setRhs(getValue(ctx.formula(1), Expression.class));
				setValue(ctx, value);
				return;
			}
			default: 
				throw new RuntimeException("unexpected binary operator: " + ctx.operator.getText() + " (line: "+ ctx.getStart().getLine() +")\n");
    	}
    }
    
    @Override
    public void exitFormulaDeclaration(FormulaDeclarationContext ctx) {
    	ExpressionDeclaration value = factory.createExpressionDeclaration();
    	value.setName(ctx.IDENTIFIER().getText());
    	value.setExpression(getValue(ctx.formula(), Expression.class));
    	setValue(ctx, value);
    }
    
    @Override
    public void exitFormulaDeclarationList(FormulaDeclarationListContext ctx) {
    	List<ExpressionDeclaration> value = new ArrayList<ExpressionDeclaration>();
    	
    	for (FormulaDeclarationContext fctx : ctx.formulaDeclaration()) {
    		value.add(getValue(fctx, ExpressionDeclaration.class));
    	}
    	setValue(ctx, value);
    }
    
    @Override
    public void exitLetExp(LetExpContext ctx) {
    	LetExpression value = factory.createLetExpression();

    	@SuppressWarnings("unchecked")
		List<ExpressionDeclaration> declarations = getValue(ctx.formulaDeclarationList(), List.class);
    	value.getDeclarations().addAll(declarations);
    	
    	value.setExpression(getValue(ctx.formula(), Expression.class));
    	
    	setValue(ctx, value);
    }
    
    @Override
    public void exitBlock(BlockContext ctx) {
    	DeclarationBlock value = factory.createDeclarationBlock();
    	
    	for (FormulaDeclarationContext fctx : ctx.formulaDeclaration()) {
    		value.getDeclarations().add(getValue(fctx, ExpressionDeclaration.class));
    	}
    	
    	setValue(ctx, value);
    }
    
    @Override
    public void exitAtomExp(AtomExpContext ctx) {
    	setValue(ctx, getValue(ctx.atom()));
    }
    
    @Override
    public void exitLiteralExp(LiteralExpContext ctx) {
    	setValue(ctx, getValue(ctx.literal()));
    }
    
    @Override
    public void exitReferenceExp(ReferenceExpContext ctx) {
    	ExpressionReference value = factory.createExpressionReference();
    	value.setName(ctx.IDENTIFIER().getText());
    	setValue(ctx, value);
    }
}
