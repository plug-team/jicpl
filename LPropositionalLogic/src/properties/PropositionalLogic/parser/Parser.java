package properties.PropositionalLogic.parser;

import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;
import properties.PropositionalLogic.PropositionalLogicModel.DeclarationBlock;
import properties.PropositionalLogic.PropositionalLogicModel.Element;
import properties.PropositionalLogic.PropositionalLogicModel.Expression;
import properties.PropositionalLogic.grammar.PropositionalLogicLexer;
import properties.PropositionalLogic.grammar.PropositionalLogicParser;
import properties.PropositionalLogic.parser.SymbolResolver.SymbolMissingException;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.util.Collections;
import java.util.function.Consumer;

public class Parser {
	
	private static Parser instance = new Parser();

    public static Expression parse(File file) throws IOException {
        ANTLRFileStream fs = new ANTLRFileStream(file.getAbsolutePath());
        return instance.parse(fs, Expression.class);
    }

    public static Expression parse(String program, Consumer<Exception> errorHandler) {
        ANTLRInputStream is = new ANTLRInputStream(program);
        return instance.parse(is, Expression.class, errorHandler);
    }
    public static Expression parse(String program) {
        ANTLRInputStream is = new ANTLRInputStream(program);

        return instance.parse(is, Expression.class);
    }
    
    public static DeclarationBlock parseBlock(File file) throws IOException {
        ANTLRFileStream fs = new ANTLRFileStream(file.getAbsolutePath());

        return instance.parse(fs, DeclarationBlock.class);
    }

    public static DeclarationBlock parseBlock(String program, Consumer<Exception> errorHandler) {
        ANTLRInputStream is = new ANTLRInputStream(program);

        return instance.parse(is, DeclarationBlock.class, errorHandler);
    }

    public static DeclarationBlock parseBlock(String program) {
        ANTLRInputStream is = new ANTLRInputStream(program);

        return instance.parse(is, DeclarationBlock.class);
    }

    public static URI generateModelXMI(Expression exp, String fileName) throws IOException {
        return instance.saveXMI(exp, fileName);
    }

    public <T extends Element> T parse(ANTLRInputStream is, Class<T> type) {
        try {
            return parse(is, type, (e) -> {});
        } catch (Error e) {
            e.printStackTrace();
            return null;
        }
    }

    public <T extends Element> T parse(ANTLRInputStream is, Class<T> type, Consumer<Exception> errorHandler) {
        PropositionalLogicLexer lexer = new PropositionalLogicLexer(is);
        lexer.addErrorListener(new BaseErrorListener() {
            @Override
            public void syntaxError(Recognizer<?, ?> recognizer, Object offendingSymbol, int line, int charPositionInLine, String msg, RecognitionException e) {
            errorHandler.accept(new ParseException(msg, charPositionInLine));
            }
        });

        CommonTokenStream tokens = new CommonTokenStream(lexer);
        PropositionalLogicParser parser = new PropositionalLogicParser(tokens);
        parser.setErrorHandler(new DefaultErrorStrategy() {
            @Override
            public void reportError(org.antlr.v4.runtime.Parser recognizer, RecognitionException e) {
                errorHandler.accept(e);
            }
        });

        ParseTree tree = type.isAssignableFrom(DeclarationBlock.class) ? parser.block() : parser.formula();
        ASTBuilder builder = new ASTBuilder();
        ParseTreeWalker walker = new ParseTreeWalker();
        walker.walk(builder, tree);

        try {
            SymbolResolver resolver = new SymbolResolver(builder.values);
            walker.walk(resolver, tree);
            return resolver.getValue(tree, type);
        } catch (SymbolMissingException e) {
            errorHandler.accept(e);
            return null;
        }
    }

	public URI saveXMI(Element object, String fileName) throws IOException {
        ResourceSet resourceSet = new ResourceSetImpl();
        resourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap().put("xmi", new XMIResourceFactoryImpl());
        Resource r = resourceSet.createResource(URI.createFileURI(fileName));
        r.getContents().add(object);
        r.save(Collections.emptyMap());
        return r.getURI();
    }
	
	public static class ErrorHandler extends BaseErrorListener {
        @Override
        public void syntaxError(Recognizer<?, ?> rec, Object offendingSymbol, int line, int column, String msg, RecognitionException e) {
            throw new RuntimeException(e.getLocalizedMessage());
        }
    }
}
