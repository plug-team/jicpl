package properties.LTL.grammar;


import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.ParseTree;
import org.junit.Test;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.lang.reflect.Method;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class LTLGrammarTest {
	private String currentRule;
	private Parser mParser;
	
	
	@Test	
	public void testAtomInline() {
		setCurrentRule("block");
		assertParse("x=(|abcd=30| && |y=20|)", "(block (formulaDeclaration x = (formula ( (formula (formula (atom |abcd=30|)) && (formula (atom |y=20|))) ))))");
	}
	@Test	
	public void testAtomDecl() {
		setCurrentRule("block");
		assertParse("x=|xyz= 30&&y=20|", "(block (formulaDeclaration x = (formula (atom |xyz= 30&&y=20|))))");
	}
	
	public ParseTree assertParse(String input, String expectation) {
		ParseTree result = parse(input, currentRule);
		assertTrue(result != null);
		
		Object actual = result.toStringTree(mParser);
		assertEquals("testing " + input + " gives " + expectation, expectation, actual);
		return result;
	}
	
	public ParseTree assertFile(String filename) {
		ParseTree result = parseFile(filename);
		assertTrue("testing "+ filename, result != null);
		return result;
	}
	
	public ParseTree parseFile(String filename) {
		ANTLRFileStream fs;
		try {
			fs = new ANTLRFileStream(filename);
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
		return parse(fs, "system");
	}
	
	public ParseTree parse(CharStream cs, String rule) {
		LTLLexer 			lexer 	= new LTLLexer(cs);
		CommonTokenStream 	tokens 	= new CommonTokenStream(lexer);
		Parser 				parser 	= new LTLParser(tokens);
		NoErrorsForTest  	errorL  = new NoErrorsForTest();

		mParser = parser;
		//parser.removeErrorListeners();
		parser.addErrorListener(errorL);
		try {
			Method mtd = parser.getClass().getMethod(rule);
			ParseTree pt = (ParseTree)mtd.invoke(parser);
			if (errorL.hasErrors()) return null;
			return pt; 
		} catch (Exception e) {
			System.err.println("no matching method for rule: " + rule);
			return null;
		}
	}
	public static class NoErrorsForTest extends BaseErrorListener {
		private Boolean hasErrors = false;
		@Override
		public void syntaxError(Recognizer<?, ?> rec, Object offendingSymbol, int line, int column, String msg, RecognitionException e) {
			hasErrors = true;
		}
		public Boolean hasErrors() {
			return hasErrors;
		}
	}
	class LTLFileFilter implements FilenameFilter {
		public boolean accept(File dir, String name) {
			return name.endsWith(".LTL");
		}
	}
	public void setCurrentRule(String currentRule) {
		this.currentRule = currentRule;
	}
	public ParseTree parse(String input, String rule) {
		ANTLRInputStream 	is		= new ANTLRInputStream(input);
		return parse(is, rule);
	}
	public ParseTree assertParse(String input) {
		ParseTree result = parse(input, currentRule);
		assertTrue(result != null);
		return result;
	}
}
