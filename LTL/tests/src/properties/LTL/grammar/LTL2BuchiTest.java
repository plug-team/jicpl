package properties.LTL.grammar;

import org.junit.Ignore;
import org.junit.Test;
import properties.BuchiAutomata.BuchiAutomataModel.Automaton;
import properties.BuchiAutomata.BuchiAutomataModel.BuchiDeclaration;
import properties.LTL.parser.Parser;
import properties.LTL.transformations.LTL2Buchi;
import properties.PropositionalLogic.PropositionalLogicModel.*;

import java.io.PrintWriter;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.assertThat;

public class LTL2BuchiTest {
    LTL2Buchi convertor = new LTL2Buchi(new PrintWriter(System.out));

    @Test
    public void testExclusion() {
        String ltl = "exclusion = []!(|Alice@CS| && |Bob@CS|)";
        DeclarationBlock propertiesBlock = Parser.parseBlock(ltl);
        Optional<Expression> property = propertiesBlock.getDeclarations().stream()
                .filter(p -> p.getName().equals("exclusion"))
                .map(p -> ((ExpressionDeclaration)p).getExpression())
                .findFirst();
        BuchiDeclaration decl = convertor.convert(property.get());
        assertThat(decl.getDeclarations().size(), is(2));
        Automaton buchi = (Automaton) decl.getAutomaton();
        assertThat(buchi.getInitial().getName(), is("T0_init"));
        assertThat(buchi.getStates().size(), is(2));
        assertThat(buchi.getTransitions().size(), is(3));
    }

    @Test
    public void testOften() {
        String code =
                "aliceInCS = |aliceState == 2|\n" +
                "bobInCS = |bobState == 3|\n" +
                "//each process can enter the critical section infinitely often\n" +
                "often = let\n" +
                "\taliceIO = [] <> aliceInCS,\n" +
                "\tbobIO = [] <> bobInCS\n" +
                "in  ! (aliceIO && bobIO)";
        DeclarationBlock propertiesBlock = Parser.parseBlock(code);
        Optional<Expression> property = propertiesBlock.getDeclarations().stream()
                .filter(p -> p.getName().equals("often"))
                .map(p -> ((ExpressionDeclaration)p).getExpression())
                .findFirst();
        BuchiDeclaration decl = convertor.convert(property.get());
        assertThat(decl.getDeclarations().size(), is(2));
        Automaton buchi = (Automaton) decl.getAutomaton();
        assertThat(buchi.getInitial().getName(), is("T0_init"));
        assertThat(buchi.getStates().size(), is(3));
        assertThat(buchi.getTransitions().size(), is(8));
    }

    @Test
    public void testGpExp() {
        Expression exp = Parser.parse("let p=|toto| in ! G p");
        BuchiDeclaration decl = convertor.convert(exp);

        assertThat(decl.getDeclarations().size(), is(1));
        Automaton buchi = (Automaton) decl.getAutomaton();
        assertThat(buchi.getInitial().getName(), is("accept_init"));
        assertThat(buchi.getStates().size(), is(1));
        assertThat(buchi.getTransitions().size(), is(1));
        assertThat(buchi.getTransitions().get(0).getGuard(), is(instanceOf(LetExpression.class)));
        assertThat(((LetExpression)buchi.getTransitions().get(0).getGuard()).getExpression(), is(instanceOf(ExpressionReference.class)));
    }

    @Test
    public void testGp() {
        Automaton buchi = convertor.convert("G p");
        assertThat(buchi.getInitial().getName(), is("accept_init"));
        assertThat(buchi.getStates().size(), is(1));
        assertThat(buchi.getTransitions().size(), is(1));
        assertThat(buchi.getTransitions().get(0).getGuard(), is(instanceOf(LetExpression.class)));
    }

    @Test @Ignore
    public void testGpAndq() {
        Automaton buchi = convertor.convert("! G p&&q");
        assertThat(buchi.getInitial().getName(), is("accept_init"));
        assertThat(buchi.getStates().size(), is(2));
        assertThat(buchi.getTransitions().size(), is(2));
        assertThat(buchi.getTransitions().get(0).getGuard(), is(instanceOf(LetExpression.class)));
        assertThat(((LetExpression)buchi.getTransitions().get(0).getGuard()).getExpression(), is(instanceOf(LogicalConjunction.class)));
        assertThat(buchi.getTransitions().get(1).getGuard(), is(instanceOf(LetExpression.class)));
        assertThat(((LetExpression)buchi.getTransitions().get(1).getGuard()).getExpression(), is(instanceOf(ExpressionReference.class)));
        assertThat(buchi.getTransitions().get(1).getFrom(), is(sameInstance(buchi.getTransitions().get(1).getTo())));
    }
}
