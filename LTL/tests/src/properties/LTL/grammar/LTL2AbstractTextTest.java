package properties.LTL.grammar;

import org.junit.Test;
import properties.LTL.parser.Parser;
import properties.LTL.transformations.LTL2AbstractText;
import properties.PropositionalLogic.PropositionalLogicModel.DeclarationBlock;
import properties.PropositionalLogic.PropositionalLogicModel.Expression;
import properties.PropositionalLogic.PropositionalLogicModel.ExpressionDeclaration;

import java.util.Optional;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;


/**
 * @author Ciprian Teodorov (ciprian.teodorov@ensta-bretagne.fr)
 *         Created on 16/03/16.
 */
public class LTL2AbstractTextTest {

    @Test
    public void testFair() {
        String fair =
                "aliceInCS = |aliceState == 2|\n" +
                "bobInCS = |bobState == 3|\n" +
                "fair = let\n" +
                "\taliceGetsIn = [] (|aliceFlag == 1| => <> aliceInCS),\n" +
                "\tbobGetsIn = [] (|bobFlag == 1| => <> bobInCS)\n" +
                "in aliceGetsIn && bobGetsIn";
        Optional<LTL2AbstractText.AbstractFormula> result = ltl2text(fair, "fair");
        assertThat(result.map(p -> p.formula).get(), is("(([] (a0 -> (<> a1))) && ([] (a2 -> (<> a3))))"));
        assertThat(result.map(p->p.atoms).isPresent(), is(notNullValue()));
        assertThat(result.map(p->p.atoms).get().size(), is(4));
    }

    @Test
    public void testIdling() {
        String fair =
                "aliceInCS = |aliceState == 2|\n" +
                "bobInCS = |bobState == 3|\n" +
                "//if one does not set the flag it does not get in the critical section\n" +
                "idling = let\n" +
                "\taliceIdle = ([] |aliceFlag == 0|) => ([] !aliceInCS),\n" +
                "\tbobIdle   = ([] |bobFlag == 0|) => ([] !bobInCS)\n" +
                "in aliceIdle || bobIdle";
        Optional<LTL2AbstractText.AbstractFormula> result = ltl2text(fair, "idling");
        assertThat(result.map(p -> p.formula).get(), is("((([] a0) -> ([] (! a1))) || (([] a2) -> ([] (! a3))))"));
        assertThat(result.map(p->p.atoms).isPresent(), is(notNullValue()));
        assertThat(result.map(p->p.atoms).get().size(), is(4));
    }

    @Test
    public void testOften() {
        String fair =
                "aliceInCS = |aliceState == 2|\n" +
                "bobInCS = |bobState == 3|\n" +
                "//each process can enter the critical section infinitely often\n" +
                "often = let\n" +
                "\taliceIO = [] <> aliceInCS,\n" +
                "\tbobIO = [] <> bobInCS\n" +
                "in aliceIO && bobIO";
        Optional<LTL2AbstractText.AbstractFormula> result = ltl2text(fair, "often");
        assertThat(result.map(p -> p.formula).get(), is("(([] (<> a0)) && ([] (<> a1)))"));
        assertThat(result.map(p->p.atoms).isPresent(), is(notNullValue()));
        assertThat(result.map(p->p.atoms).get().size(), is(2));
    }

    Optional<LTL2AbstractText.AbstractFormula> ltl2text(String properties, String propertyName) {
        DeclarationBlock propertiesBlock = Parser.parseBlock(properties);
        Optional<Expression> property = propertiesBlock.getDeclarations().stream()
                .filter(p -> p.getName().equals(propertyName))
                .map(p -> ((ExpressionDeclaration)p).getExpression())
                .findFirst();

        Optional<LTL2AbstractText.AbstractFormula> formula = property.map(e -> new LTL2AbstractText().convert(e));

        return formula;
    }
}
