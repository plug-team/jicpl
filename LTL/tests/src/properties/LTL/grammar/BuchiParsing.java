package properties.LTL.grammar;

import org.junit.Test;
import properties.LTL.parser.Parser;
import properties.PropositionalLogic.PropositionalLogicModel.DeclarationBlock;

public class BuchiParsing {
    @Test
    public void test1() {
        String ltl = "aut1 = \n" +
                "    states s0, s1;\n" +
                "    initial s0;\n" +
                "    accept s1;\n" +
                "    s0 [ |a=5| ] s1;\n" +
                "    s0 [ true ] s0;\n" +
                "    s1 [ |b=7| and |c=2| ] s1";
        DeclarationBlock propertiesBlock = Parser.parseBlock(ltl);

        //Assert.assertThat();
        System.out.print("too");
    }

    @Test
    public void test2() {
        String ltl = "b1 = let\n" +
                "\t\ta1 = |P_1!=wait|,\n" +
                "\t\ta2 = |P_0=choose|,\n" +
                "\t\ta3 = |P_2!=choose|,\n" +
                "\t\ta4 = |number[1]<0|,\n" +
                "\t\tp1 = !a3 and !a4,\n" +
                "\t\tp2 = a3 and a4,\n" +
                "\tin\n" +
                "\t\tstates s0, s1, s2, s3;\n" +
                "\t    initial s0;\n" +
                "\t\taccept s2 s3;\n" +
                "\t\ts0 [!a2 and !a1] s1;\n" +
                "\t\ts0 [(!a1 and p1) or (!a1 and p2)] s2;\n" +
                "\t\ts0 [a1] s3;\n" +
                "\t\ts1 [!a2] s1;\n" +
                "\t\ts1 [|P_1==wait|] s2;\n" +
                "\t\ts2 [p1] s2;\n" +
                "\t\ts3 [true] s3";
        DeclarationBlock propertiesBlock = Parser.parseBlock(ltl);

        //Assert.assertThat();
        System.out.print("too");
    }
}
