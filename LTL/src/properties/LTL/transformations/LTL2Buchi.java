package properties.LTL.transformations;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.eclipse.emf.ecore.util.EcoreUtil;
import properties.BuchiAutomata.BuchiAutomataModel.*;
import properties.LTL.ltl3ba.LTL3BA;
import properties.LTL.parser.Parser;
import properties.PropositionalLogic.PropositionalLogicModel.DeclarationBlock;
import properties.PropositionalLogic.PropositionalLogicModel.*;

import java.io.PrintWriter;
import java.util.*;
import java.util.stream.Collectors;

public class LTL2Buchi {
	private final PrintWriter out;
	public LTL2Buchi(final PrintWriter out) {
		this.out = out == null ? new PrintWriter (System.out) :  out;
	}

	public BuchiDeclaration getBuchiDeclaration(String formula) throws Exception {
		return getBuchiDeclaration(null, new ANTLRInputStream(formula));
	}

	public BuchiDeclaration getBuchiDeclaration(String name, String formula) throws Exception {
		return getBuchiDeclaration(name, new ANTLRInputStream(formula));
	}

	public BuchiDeclaration getBuchiDeclaration(String name, ANTLRInputStream is) throws Exception {
		DeclarationBlock block = getDeclarationBlock(is);

		if (name == null) {
			if (block.getDeclarations().size() > 1) {
				throw new PropertyNotFoundException("Multiple properties found in " + is.getSourceName() + ", only one supported");
			}
			if (block.getDeclarations().size() == 0) {
				throw new PropertyNotFoundException("No properties found in " + is.getSourceName());
			}
			if (block.getDeclarations().get(0) instanceof BuchiDeclaration) {
				return (BuchiDeclaration) block.getDeclarations().get(0);
			}
			if (block.getDeclarations().get(0) instanceof ExpressionDeclaration) {
				BuchiDeclaration decl = convert(((ExpressionDeclaration) block.getDeclarations().get(0)).getExpression());
				decl.setName(name);
				return decl;
			}
		}

		for (Declaration declaration : block.getDeclarations()) {
			if (declaration.getName().equals(name)) {
				if (declaration instanceof BuchiDeclaration) {
					return (BuchiDeclaration)declaration;
				}
				if (declaration instanceof ExpressionDeclaration) {
					BuchiDeclaration decl = convert(((ExpressionDeclaration) declaration).getExpression());
					decl.setName(name);
					return decl;
				}
			}
		}
		throw new PropertyNotFoundException("Could not find the property \"" + name + "\" in " + is.getSourceName());
	}

	public Collection<BuchiDeclaration> getBuchiDeclarations(ANTLRInputStream is) throws Exception {
		Collection<BuchiDeclaration> declarations = new ArrayList<>();
		DeclarationBlock block = getDeclarationBlock(is);
		for (Declaration declaration : block.getDeclarations()) {
			if (declaration instanceof BuchiDeclaration) {
				declarations.add((BuchiDeclaration)declaration);
			}
			if (declaration instanceof ExpressionDeclaration) {
				BuchiDeclaration decl = convert(((ExpressionDeclaration) declaration).getExpression());
				decl.setName(declaration.getName());
				declarations.add(decl);
			}
		}
		return declarations;
	}


	public DeclarationBlock getDeclarationBlock(ANTLRInputStream is) throws Exception {
		final List<Exception> exceptions = new ArrayList<>();
		DeclarationBlock block = new Parser().parse(is, DeclarationBlock.class, exceptions::add);
		if (exceptions.size() > 0) {
			throw exceptions.get(0);
		}

		return block;
	}

	public static class PropertyNotFoundException extends Exception {
		public PropertyNotFoundException(String message) {
			super(message);
		}
	}

	public BuchiDeclaration convert(Declaration declaration) {
		if (declaration instanceof ExpressionDeclaration) {
			BuchiDeclaration buchi = convert(((ExpressionDeclaration)declaration).getExpression());
			buchi.setName(declaration.getName());
			return buchi;
		}
		if (declaration instanceof BuchiDeclaration) {
			return (BuchiDeclaration) declaration;
		}
		return null;
	}

	public BuchiDeclaration convert(Expression exp) {
		LTL2AbstractText.AbstractFormula absFormula = new LTL2AbstractText().convert(exp);

		out.println("\n\nTransforming LTL formula : !" + absFormula.formula);
		out.flush();

		List<String[]> transitions = LTL3BA.transform("!(" + absFormula.formula + ")");

		Map<String, ExpressionDeclaration> atomMap = absFormula.atoms.entrySet().stream()
				.collect(Collectors.toMap(
						p -> p.getKey(),
						p -> {ExpressionDeclaration eD = pFactory.createExpressionDeclaration();
							eD.setName(p.getKey());
							eD.setExpression(EcoreUtil.copy(p.getValue()));
							return eD;}
				));


		Automaton buchi = readBuchiFromSpot(transitions, atomMap);

		BuchiDeclaration result = bFactory.createBuchiDeclaration();
		result.getDeclarations().addAll(atomMap.values());
		result.setAutomaton(buchi);
		return result;
	}

	public Automaton convert(String exp) {
		out.println("\n\nTransforming LTL formula : " + exp);
		out.flush();

		List<String[]> transitions = LTL3BA.transform(exp);

		return readBuchiFromSpot(transitions, null);
	}

	PropositionalLogicModelFactory pFactory = PropositionalLogicModelFactory.eINSTANCE;
	BuchiAutomataModelFactory bFactory = BuchiAutomataModelFactory.eINSTANCE;
	
	Automaton readBuchiFromSpot(List<String[]> transitions, Map<String, ExpressionDeclaration> atomMap) {
		Map<String, State> name2state = new HashMap<String, State>();
		
		Automaton automaton = bFactory.createAutomaton();
		
		for (String[] transition : transitions) {
			GuardedTransition buchiTransition;

			State source = name2state.get(transition[0]);
			if (source == null) {
				source = createState(transition[0]);
				name2state.put(transition[0], source);
				automaton.getStates().add(source);
				if (transition[0].endsWith("init")) {
					automaton.setInitial(source);
				}
			}
			
			State target = name2state.get(transition[1]);
			if (target == null) {
				target = createState(transition[1]);
				name2state.put(transition[1], target);
				automaton.getStates().add(target);
			}
			
			Expression guard = parsePredicateFromSpot(transition[2], atomMap);
			
			buchiTransition = bFactory.createGuardedTransition();
			buchiTransition.setFrom(source);
			buchiTransition.setTo(target);
			if (guard != null) {
				LetExpression letE = pFactory.createLetExpression();
				if (atomMap != null) {
					letE.getDeclarations().addAll(EcoreUtil.copyAll(atomMap.values()));
				}
				letE.setExpression(guard);
				buchiTransition.setGuard(letE);
			}
			
			automaton.getTransitions().add(buchiTransition);
		}
		
		
		return automaton;
	}
	
	State createState(String name) {
		State state;
		if (name.startsWith("accept")) {
			state = bFactory.createAcceptingState();
		}
		else {
			state = bFactory.createState();
		}
		state.setName(name);
		return state;
	}

	Expression parsePredicateFromSpot(final String guardString, final Map<String, ExpressionDeclaration> atomMap) {

		class Parser {
			int position = 0;

			void next() {
				position++;
				while (position < guardString.length() && Character.isWhitespace(getChar())) {
					position++;
				}
			}

			Expression parse() {
				if (guardString.equals("true")) return pFactory.createTrue();
				return parseExpression();
			}

			Expression parseExpression() {
				Expression lhs = parseUnary();

				while (position < guardString.length()) {
					if (getChar() == '&') {
						next();
						if (getChar() != '&') throw new RuntimeException("expected '&&' but found '&" + getChar() + "'");
						next();
						Expression rhs = parseUnary();

						LogicalConjunction predicate = pFactory.createLogicalConjunction();
						predicate.setOperatorToken("&&");
						predicate.setLhs(lhs);
						predicate.setRhs(rhs);
						lhs = predicate;
					} else if (getChar() == '|') {
						next();
						if (getChar() != '|') throw new RuntimeException("expected '||' but found '|" + getChar() + "'");
						next();
						Expression rhs = parseUnary();

						LogicalDisjunction predicate = pFactory.createLogicalDisjunction();
						predicate.setOperatorToken("||");
						predicate.setLhs(lhs);
						predicate.setRhs(rhs);
						lhs = predicate;
					} else {
						return lhs;
					}
				}
				return lhs;
			}

			Expression parseUnary() {
				boolean negation = false;
				Expression result;
				if (getChar() == '!') {
					//create negation node
					next();
					negation = true;
				}

				if (getChar() == '(') {
					next();
					result = parseExpression();
					if (getChar() == ')') { next(); }
				}
				else {
					StringBuilder identSB = new StringBuilder();
					while (		(getChar() >= 'a' && getChar() <= 'z')
							||	(getChar() >= '0' && getChar() <= '9')
							||	(getChar() == '_') ) {
						identSB.append(getChar());
						next();
					}
					if (identSB.length() == 0) throw new RuntimeException("unexpected character " + getChar() + " in buchi guard expression");
					ExpressionReference reference = pFactory.createExpressionReference();
					reference.setName(identSB.toString());

					if (atomMap != null) {
						String name = identSB.toString();
						ExpressionDeclaration decl = atomMap.get(name);
						reference.setExp(decl);
					}

					result = reference;
				}
				if (negation) {
					LogicalNegation predicate = pFactory.createLogicalNegation();
					predicate.setOperatorToken("!");
					predicate.setOperand(result);
					return predicate;
				}
				return result;
			}

			private char getChar() {
				return guardString.charAt(position);
			}
		}
		return (new Parser()).parse();
	}
}
