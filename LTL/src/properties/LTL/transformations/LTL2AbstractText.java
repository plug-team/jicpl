package properties.LTL.transformations;

import properties.LTL.LTLModel.*;
import properties.LTL.LTLModel.util.LTLModelSwitch;
import properties.PropositionalLogic.PropositionalLogicModel.Atom;
import properties.PropositionalLogic.PropositionalLogicModel.Expression;
import properties.PropositionalLogic.PropositionalLogicModel.ExpressionDeclaration;
import properties.PropositionalLogic.transformations.PropositionalLogic2AbstractText;

import java.util.HashMap;
import java.util.Map;

public class LTL2AbstractText {
	public class AbstractFormula {
		public Map<String, Atom> atoms = new HashMap<>();
		public String formula = "";
	}
	
	public AbstractFormula convert(ExpressionDeclaration formula) {
		return convert(formula.getExpression());
	}
	public AbstractFormula convert(Expression formula) {
		AbstractFormula result = new AbstractFormula();
		propSwitch.name2atom = result.atoms;
		result.formula = propSwitch.doSwitch(formula);
		return result;
	}
	
	final PropositionalLogic2AbstractText propSwitch = new PropositionalLogic2AbstractText() {
		@Override
		public String defaultCase(org.eclipse.emf.ecore.EObject object) {
			return ltlSwitch.doSwitch(object);
		}
	};
	
	final LTLModelSwitch<String> ltlSwitch = new LTLModelSwitch<String>() {
        @Override
		public String caseEventually(Eventually object) {
			String operand = doSwitch(object.getOperand());
			return "(<> " + operand + ")";
		}
        @Override
		public String caseGlobally(Globally object) {
			String operand = doSwitch(object.getOperand());
			return "([] " + operand + ")";
		}
        @Override
		public String caseNext(Next object) {
			String operand = doSwitch(object.getOperand());
			return "(X " + operand + ")";
		}
        @Override
		public String caseStrongUntil(StrongUntil object) {
			String lhs = doSwitch(object.getLhs());
			String rhs = doSwitch(object.getRhs());
			return "(" + lhs + " U " + rhs + ")";
		}
        @Override
		public String caseWeakUntil(WeakUntil object) {
			String lhs = doSwitch(object.getLhs());
			String rhs = doSwitch(object.getRhs());
			//return "(" + lhs + " W " + rhs + ")"; // not supported by ltl3ba
			return "(([] " + lhs + " ) || ( " + lhs + " U " + rhs + " ))";
		}
        @Override
		public String caseStrongRelease(StrongRelease object) {
			String lhs = doSwitch(object.getLhs());
			String rhs = doSwitch(object.getRhs());
			//return "(" + lhs + " M " + rhs + ")"; // not supported by ltl3ba
			return "( " + rhs + " U ( " + lhs + " && " + rhs + " ))";
		}
        @Override
		public String caseWeakRelease(WeakRelease object) {
			String lhs = doSwitch(object.getLhs());
			String rhs = doSwitch(object.getRhs());
			return "(" + lhs + " R " + rhs + ")";
		}
        @Override
		public String defaultCase(org.eclipse.emf.ecore.EObject object) {
			return propSwitch.doSwitch(object);
		}
	};
}
