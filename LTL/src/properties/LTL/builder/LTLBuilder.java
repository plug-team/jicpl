package properties.LTL.builder;

import properties.LTL.LTLModel.*;
import properties.PropositionalLogic.PropositionalLogicModel.*;

public class LTLBuilder {
	public static final LTLBuilder instance = new LTLBuilder();
	LTLModelFactory ltlFactory = LTLModelFactory.eINSTANCE;
	PropositionalLogicModelFactory propFactory = PropositionalLogicModelFactory.eINSTANCE;
	
	public Atom atom(String code, String language) {
		Atom at = propFactory.createAtom();
		at.setCode(code);
		at.setLanguage(language);
		return at;
	}
	
	public Atom atom(String code) {
		return atom(code, null);
	}

	public True ltlTrue() {
		return propFactory.createTrue();
	}
	
	public False ltlFalse() {
		return propFactory.createFalse();
	}
	
	public LogicalConjunction conjunction(Expression lhs, Expression rhs) {
		LogicalConjunction exp = propFactory.createLogicalConjunction();
		exp.setLhs(lhs);
		exp.setRhs(rhs);
		return exp;
	}
	
	public LogicalConjunction and(Expression lhs, Expression rhs) {
		return conjunction(lhs, rhs);
	}
	
	public LogicalDisjunction disjunction(Expression lhs, Expression rhs) {
		LogicalDisjunction exp = propFactory.createLogicalDisjunction();
		exp.setLhs(lhs);
		exp.setRhs(rhs);
		return exp;
	}
	
	public LogicalDisjunction or(Expression lhs, Expression rhs) {
		return disjunction(lhs, rhs);
	}
	
	public LogicalEquivalence equivalence(Expression lhs, Expression rhs) {
		LogicalEquivalence exp = propFactory.createLogicalEquivalence();
		exp.setLhs(lhs);
		exp.setRhs(rhs);
		return exp;
	}
	
	public LogicalImplication implication(Expression lhs, Expression rhs) {
		LogicalImplication exp = propFactory.createLogicalImplication();
		exp.setLhs(lhs);
		exp.setRhs(rhs);
		return exp;
	}
	
	public StrongRelease strongRelease(Expression lhs, Expression rhs) {
		StrongRelease exp = ltlFactory.createStrongRelease();
		exp.setLhs(lhs);
		exp.setRhs(rhs);
		return exp;
	}
	public WeakRelease weakRelease(Expression lhs, Expression rhs) {
		WeakRelease exp = ltlFactory.createWeakRelease();
		exp.setLhs(lhs);
		exp.setRhs(rhs);
		return exp;
	}
	
	public StrongUntil strongUntil(Expression lhs, Expression rhs) {
		StrongUntil exp = ltlFactory.createStrongUntil();
		exp.setLhs(lhs);
		exp.setRhs(rhs);
		return exp;
	}
	
	public WeakUntil weakUntil(Expression lhs, Expression rhs) {
		WeakUntil exp = ltlFactory.createWeakUntil();
		exp.setLhs(lhs);
		exp.setRhs(rhs);
		return exp;
	}
	
	public LogicalNegation negation(Expression value) {
		LogicalNegation exp = propFactory.createLogicalNegation();
		exp.setOperand(value);
		return exp;
	}
	
	public LogicalNegation not(Expression value) {
		return negation(value);
	}
	
	public Eventually eventually(Expression value) {
		Eventually exp = ltlFactory.createEventually();
		exp.setOperand(value);
		return exp;
	}
	
	public Globally globally(Expression value) {
		Globally exp = ltlFactory.createGlobally();
		exp.setOperand(value);
		return exp;
	}
	
	public Next next(Expression value) {
		Next exp = ltlFactory.createNext();
		exp.setOperand(value);
		return exp;
	}
	
	public ExpressionDeclaration formula(String name, Expression value) {
		ExpressionDeclaration formula = propFactory.createExpressionDeclaration();
		formula.setName(name);
		formula.setExpression(value);
		return formula;
	}
}
