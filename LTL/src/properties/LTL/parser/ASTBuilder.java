package properties.LTL.parser;

import org.antlr.v4.runtime.tree.ParseTreeProperty;
import org.antlr.v4.runtime.tree.TerminalNode;
import properties.BuchiAutomata.BuchiAutomataModel.*;
import properties.LTL.LTLModel.*;
import properties.LTL.grammar.LTLBaseListener;
import properties.LTL.grammar.LTLParser;
import properties.LTL.grammar.LTLParser.*;
import properties.PropositionalLogic.PropositionalLogicModel.DeclarationBlock;
import properties.PropositionalLogic.PropositionalLogicModel.*;
import properties.PropositionalLogic.parser.TreeAnnotator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ASTBuilder extends LTLBaseListener implements TreeAnnotator {
	public ParseTreeProperty<Object> values = new ParseTreeProperty<>();
	public ParseTreeProperty<Object> getValues() {
		return values;
	};
	
    PropositionalLogicModelFactory factory = PropositionalLogicModelFactory.eINSTANCE;
    
    @Override
    public void exitLiteral(LiteralContext ctx) {    
    	//ideally true and false should be singletons, but there are containment problems
        setValue(ctx, ctx.TRUE() == null ? factory.createFalse() : factory.createTrue());
    }
    
    @Override
    public void exitAtom(AtomContext ctx) {
    	Atom atom = factory.createAtom();
    	String language = ctx.IDENTIFIER() != null ? ctx.IDENTIFIER().getText() : null;
    	atom.setLanguage(language);
    	String code = ctx.ATOMINLINE().getText();
    	//remove the '|' or '"' at the beginning and at the end
    	atom.setCode(code.substring(1, code.length()-1));
    	
    	atom.setDelimiterString(code.substring(0,1));
    	setValue(ctx, atom);
    }
    
    @Override
    public void exitParenExp(ParenExpContext ctx) {
    	setValue(ctx, getValue(ctx.formula()));
    }
    
    LTLModelFactory ltlFactory = LTLModelFactory.eINSTANCE;
    
    @Override
    public void exitUnaryExp(UnaryExpContext ctx) {
    	switch (ctx.operator.getType()) {
    	case LTLParser.NEGATION: {
	    	LogicalNegation value = factory.createLogicalNegation();
	    	value.setOperatorToken(ctx.operator.getText());
	    	value.setOperand(getValue(ctx.formula(), Expression.class));
	    	setValue(ctx, value);
	    	return;
    	}
    	case LTLParser.NEXT: {
    		Next value = ltlFactory.createNext();
    		value.setOperatorToken(ctx.operator.getText());
    		value.setOperand(getValue(ctx.formula(), Expression.class));
    		setValue(ctx, value);
    		return;
    	}
    	case LTLParser.EVENTUALLY: {
    		Eventually value = ltlFactory.createEventually();
    		value.setOperatorToken(ctx.operator.getText());
    		value.setOperand(getValue(ctx.formula(), Expression.class));
    		setValue(ctx, value);
    		return;
    	}
    	case LTLParser.GLOBALLY: {
    		Globally value = ltlFactory.createGlobally();
    		value.setOperatorToken(ctx.operator.getText());
    		value.setOperand(getValue(ctx.formula(), Expression.class));
    		setValue(ctx, value);
    		return;
    	}
    	}
    }
    
    @Override
    public void exitBinaryExp(BinaryExpContext ctx) {
    	switch (ctx.operator.getType()) {
			case LTLParser.CONJUNCTION: {
				LogicalConjunction value = factory.createLogicalConjunction();
				value.setOperatorToken(ctx.operator.getText());
				value.setLhs(getValue(ctx.formula(0), Expression.class));
				value.setRhs(getValue(ctx.formula(1), Expression.class));
				setValue(ctx, value);
    			return;
			}
			case LTLParser.DISJUNCTION: {
				LogicalDisjunction value = factory.createLogicalDisjunction();
				value.setOperatorToken(ctx.operator.getText());
				value.setLhs(getValue(ctx.formula(0), Expression.class));
				value.setRhs(getValue(ctx.formula(1), Expression.class));
				setValue(ctx, value);
				return;
			}
			case LTLParser.XOR: {
				LogicalXOR value = factory.createLogicalXOR();
				value.setOperatorToken(ctx.operator.getText());
				value.setLhs(getValue(ctx.formula(0), Expression.class));
				value.setRhs(getValue(ctx.formula(1), Expression.class));
				setValue(ctx, value);
				return;
			}
			case LTLParser.IMPLICATION: {
				LogicalImplication value = factory.createLogicalImplication();
				value.setOperatorToken(ctx.operator.getText());
				value.setLhs(getValue(ctx.formula(0), Expression.class));
				value.setRhs(getValue(ctx.formula(1), Expression.class));
				setValue(ctx, value);
				return;
			}
			case LTLParser.EQUIVALENCE: {
				LogicalEquivalence value = factory.createLogicalEquivalence();
				value.setOperatorToken(ctx.operator.getText());
				value.setLhs(getValue(ctx.formula(0), Expression.class));
				value.setRhs(getValue(ctx.formula(1), Expression.class));
				setValue(ctx, value);
				return;
			}
			case LTLParser.SUNTIL: {
				StrongUntil value = ltlFactory.createStrongUntil();
				value.setOperatorToken(ctx.operator.getText());
				value.setLhs(getValue(ctx.formula(0), Expression.class));
				value.setRhs(getValue(ctx.formula(1), Expression.class));
				setValue(ctx, value);
				return;
			}
			case LTLParser.WUNTIL: {
				WeakUntil value = ltlFactory.createWeakUntil();
				value.setOperatorToken(ctx.operator.getText());
				value.setLhs(getValue(ctx.formula(0), Expression.class));
				value.setRhs(getValue(ctx.formula(1), Expression.class));
				setValue(ctx, value);
				return;
			}
			case LTLParser.SRELEASE: {
				StrongRelease value = ltlFactory.createStrongRelease();
				value.setOperatorToken(ctx.operator.getText());
				value.setLhs(getValue(ctx.formula(0), Expression.class));
				value.setRhs(getValue(ctx.formula(1), Expression.class));
				setValue(ctx, value);
				return;
			}
			case LTLParser.WRELEASE: {
				WeakRelease value = ltlFactory.createWeakRelease();
				value.setOperatorToken(ctx.operator.getText());
				value.setLhs(getValue(ctx.formula(0), Expression.class));
				value.setRhs(getValue(ctx.formula(1), Expression.class));
				setValue(ctx, value);
				return;
			}
			default: 
				throw new RuntimeException("unexpected binary operator: " + ctx.operator.getText() + " (line: "+ ctx.getStart().getLine() +")\n");
    	}
    }
    
    @Override
    public void exitFormulaDeclaration(FormulaDeclarationContext ctx) {
    	if (ctx.formula() != null) {
			ExpressionDeclaration value = factory.createExpressionDeclaration();
			value.setName(ctx.IDENTIFIER().getText());
			value.setExpression(getValue(ctx.formula(), Expression.class));
			setValue(ctx, value);
			return;
		}
    	if (ctx.buchiDecl() != null) {
    		BuchiDeclaration value = getValue(ctx.buchiDecl(), BuchiDeclaration.class);
    		value.setName(ctx.IDENTIFIER().getText());
    		setValue(ctx, value);
    		return;
		}
    }
    
    @Override
    public void exitFormulaDeclarationList(FormulaDeclarationListContext ctx) {
    	List<ExpressionDeclaration> value = new ArrayList<>();
    	
    	for (FormulaDeclarationContext fctx : ctx.formulaDeclaration()) {
    		value.add(getValue(fctx, ExpressionDeclaration.class));
    	}
    	setValue(ctx, value);
    }

	@Override
	public void exitLetDecl(LetDeclContext ctx) {
		LetExpression value = factory.createLetExpression();

		@SuppressWarnings("unchecked")
		List<ExpressionDeclaration> declarations = getValue(ctx.formulaDeclarationList(), List.class);
		value.getDeclarations().addAll(declarations);

		setValue(ctx, value);
	}

	@Override
    public void exitLetExp(LetExpContext ctx) {
    	LetExpression value = getValue(ctx.letDecl(), LetExpression.class);
    	
    	value.setExpression(getValue(ctx.formula(), Expression.class));
    	
    	setValue(ctx, value);
    }
    
    @Override
    public void exitBlock(BlockContext ctx) {
    	DeclarationBlock value = factory.createDeclarationBlock();
    	
    	for (FormulaDeclarationContext fctx : ctx.formulaDeclaration()) {
    		value.getDeclarations().add(getValue(fctx, Declaration.class));
    	}
    	
    	setValue(ctx, value);
    }
    
    @Override
    public void exitAtomExp(AtomExpContext ctx) {
    	setValue(ctx, getValue(ctx.atom()));
    }
    
    @Override
    public void exitLiteralExp(LiteralExpContext ctx) {
    	setValue(ctx, getValue(ctx.literal()));
    }
    
    @Override
    public void exitReferenceExp(ReferenceExpContext ctx) {
    	ExpressionReference value = factory.createExpressionReference();
    	value.setName(ctx.IDENTIFIER().getText());
    	setValue(ctx, value);
    }

    final BuchiAutomataModelFactory buchiFactory = BuchiAutomataModelFactory.eINSTANCE;
	@Override
	public void exitStateDecl(StateDeclContext ctx) {
		Map<String, State> value = new HashMap<>();

		for (TerminalNode idT: ctx.IDENTIFIER()) {
			State state = buchiFactory.createState();
			state.setName(idT.getText());
			value.put(idT.getText(), state);
		}
		setValue(ctx, value);
	}

	@Override
	public void exitInitialDecl(InitialDeclContext ctx) {
		List<String> value = new ArrayList<>();

		for (TerminalNode idT: ctx.IDENTIFIER()) {
			value.add(idT.getText());
		}
		setValue(ctx, value);
	}

	@Override
	public void exitAcceptDecl(AcceptDeclContext ctx) {
		List<String> value = new ArrayList<>();

		for (TerminalNode idT: ctx.IDENTIFIER()) {
			value.add(idT.getText());
		}
		setValue(ctx, value);
	}

	@Override
	public void exitTransitionDecl(TransitionDeclContext ctx) {
		GuardedTransition transition = buchiFactory.createGuardedTransition();

		State from = buchiFactory.createState();
		from.setName(ctx.IDENTIFIER(0).getText());
		transition.setFrom(from);

		State to = buchiFactory.createState();
		to.setName(ctx.IDENTIFIER(1).getText());
		transition.setTo(to);

		transition.setGuard(getValue(ctx.formula(), Expression.class));

		setValue(ctx, transition);
	}

	@Override
	public void exitAutomatonDecl(AutomatonDeclContext ctx) {
		Automaton automaton = buchiFactory.createAutomaton();

		Map<String, State> states = getValue(ctx.stateDecl(), Map.class);
		List<String> initialStates = getValue(ctx.initialDecl(), List.class);
		List<String> acceptStates = getValue(ctx.acceptDecl(), List.class);

        if (initialStates.size() != 1) {
            throw new RuntimeException("Multiple initial states are not supported yet!");
        }

		//replace the accepting states
		for (String aS : acceptStates) {
			State state = states.get(aS);
			if (state == null) {
			    throw new RuntimeException("The accepting state "+aS+" is not declared in the states statement!");
            }
			AcceptingState acceptingState = buchiFactory.createAcceptingState();
			acceptingState.setName(aS);
			states.put(aS, acceptingState);
		}

		List<State> initialStateList = new ArrayList<>();
		for (String iS : initialStates) {
		    State state = states.get(iS);
            if (state == null) {
                throw new RuntimeException("The initial state "+iS+" is not declared in the states statement!");
            }
		    initialStateList.add(state);
        }

        automaton.getStates().addAll(states.values());
		automaton.setInitial(initialStateList.get(0));

		for (TransitionDeclContext transitionDeclContext : ctx.transitionDecl()) {
			GuardedTransition transition = getValue(transitionDeclContext, GuardedTransition.class);
			State iFrom = transition.getFrom();
			State from = states.get(iFrom.getName());
			transition.setFrom(from);

			State iTo = transition.getTo();
			State to = states.get(iTo.getName());
			transition.setTo(to);

			automaton.getTransitions().add(transition);
		}
        setValue(ctx, automaton);
	}

	@Override
	public void exitBuchiDecl(BuchiDeclContext ctx) {
		BuchiDeclaration buchiDeclaration = buchiFactory.createBuchiDeclaration();
		if (ctx.letDecl() != null) {
			LetExpression let = getValue(ctx.letDecl(), LetExpression.class);
			buchiDeclaration.getDeclarations().addAll(let.getDeclarations());
		}
		buchiDeclaration.setAutomaton(getValue(ctx.automatonDecl(), Automaton.class));
		setValue(ctx, buchiDeclaration);
	}
}