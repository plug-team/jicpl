package properties.LTL.parser;

import org.antlr.v4.runtime.tree.ParseTreeProperty;
import properties.LTL.grammar.LTLBaseListener;
import properties.LTL.grammar.LTLParser.BlockContext;
import properties.LTL.grammar.LTLParser.FormulaDeclarationContext;
import properties.LTL.grammar.LTLParser.LetExpContext;
import properties.LTL.grammar.LTLParser.ReferenceExpContext;
import properties.PropositionalLogic.PropositionalLogicModel.Declaration;
import properties.PropositionalLogic.PropositionalLogicModel.ExpressionDeclaration;
import properties.PropositionalLogic.PropositionalLogicModel.ExpressionReference;
import properties.PropositionalLogic.parser.TreeAnnotator;

import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

public class SymbolResolver extends LTLBaseListener implements TreeAnnotator {
	public ParseTreeProperty<Object> values;
	Stack<Map<String, Declaration>> scopes = new Stack<Map<String,Declaration>>();
	
	public SymbolResolver(ParseTreeProperty<Object> ast) {
		this(ast, null);
	}
	
	public SymbolResolver(ParseTreeProperty<Object> ast, Map<String, Declaration> globalScope) {
		this.values = ast;
		if (globalScope != null) {
			this.scopes.push(globalScope);
		}
	}
	
	public ParseTreeProperty<Object> getValues() {
		return values;
	};
	
	//scope helper methods
	public Map<String, Declaration> currentScope() {
		return scopes.peek();
	}
	public static class SymbolMissingException extends RuntimeException {
		private static final long serialVersionUID = -342580227434108373L;

		public SymbolMissingException(String string) {
			super(string);
		}
	}
	public static class SymbolRedefinitionException extends RuntimeException {
		private static final long serialVersionUID = -5764411755432232219L;

		public SymbolRedefinitionException(String string) {
			super(string);
		}
	}

	public Declaration resolve(String name) {
		for (int i = scopes.size()-1; i>=0; i--) {
			Declaration value = scopes.get(i).get(name);
			if (value != null) return value; // symbol was found
		}
		throw new SymbolMissingException("Symbol " + name + " is not present in scope");
    }
	
	public void define(String symbol, Declaration value) {
		Declaration previousDeclaration = currentScope().get(symbol);
        if (previousDeclaration == null) {
            currentScope().put(symbol, value);
            return;
        }
        throw new SymbolRedefinitionException("Illegal redefinition of " + symbol + "[" + previousDeclaration.getClass().getSimpleName() + "] as " + value.getClass().getSimpleName());
	}
	public Map<String, Declaration> pushNewScope() {
		Map<String, Declaration> newScope = new HashMap<String, Declaration>();
		return scopes.push(newScope);
	}
	
	public Map<String, Declaration> popScope() {
		if (scopes.size() < 1) {
			throw new RuntimeException("Empty scope stack, cannot pop the scope");
		}
		return scopes.pop();
	}
    
    @Override
    public void enterLetExp(LetExpContext ctx) {
    	pushNewScope();
    }
    
    @Override
    public void exitLetExp(LetExpContext ctx) {
    	popScope();
    }
    
    @Override
    public void enterBlock(BlockContext ctx) {
    	pushNewScope();
    }
    
    @Override
    public void exitBlock(BlockContext ctx) {
    	popScope();
    }
    
    public static class InvalidDeclarationTypeException extends RuntimeException {
		private static final long serialVersionUID = -5373544148436894156L;

		public InvalidDeclarationTypeException(String string) {
			super(string);
		}
	}
    
    @Override
    public void exitFormulaDeclaration(FormulaDeclarationContext ctx) {
    	String symbol = ctx.IDENTIFIER().getText();
    	ExpressionDeclaration astNode = getValue(ctx, ExpressionDeclaration.class);
    	define(symbol, astNode);
    }
    
    @Override
    public void exitReferenceExp(ReferenceExpContext ctx) {
    	//try to resolve the name
    	Declaration declaration = resolve(ctx.IDENTIFIER().getText());
    	if (!(declaration instanceof ExpressionDeclaration)) {
    		throw new InvalidDeclarationTypeException("The declared identifier " + declaration.getName() + "[" + declaration.getClass().getSimpleName() + "]"+" is not an instance of " + ExpressionDeclaration.class.getSimpleName());
    	}
    	//get the value associated to the AST
    	ExpressionReference ref = getValue(ctx, ExpressionReference.class);
    	ref.setExp((ExpressionDeclaration)declaration);
    }
}
