grammar LTL;

block : formulaDeclaration+;

formulaDeclaration : IDENTIFIER EQ (formula | buchiDecl);
formulaDeclarationList : formulaDeclaration (COMMA formulaDeclaration)* COMMA?;

formula :
		  literal													                #LiteralExp
		| IDENTIFIER												                #ReferenceExp
		| atom														                #AtomExp
		| LPAREN formula RPAREN 									                #ParenExp
		| operator=NEGATION formula											        #UnaryExp
		| operator=NEXT formula                                                     #UnaryExp
		| operator=(EVENTUALLY|GLOBALLY) formula                                    #UnaryExp
		|<assoc=right> formula operator=(SUNTIL|WUNTIL|SRELEASE|WRELEASE) formula   #BinaryExp
		| formula operator=CONJUNCTION formula						                #BinaryExp
		| formula operator=DISJUNCTION formula						                #BinaryExp
		| formula operator=XOR formula								                #BinaryExp
		|<assoc=right> formula operator=(IMPLICATION | EQUIVALENCE) formula         #BinaryExp
		| letDecl formula					        	                            #LetExp
		;

literal : TRUE | FALSE;
atom : IDENTIFIER? ATOMINLINE;

letDecl : LET formulaDeclarationList IN;

//buchi automata
stateDecl : STATES IDENTIFIER (',' IDENTIFIER)*;
initialDecl : INITIAL IDENTIFIER (',' IDENTIFIER)*;
acceptDecl : ACCEPT IDENTIFIER (',' IDENTIFIER)*;
transitionDecl : IDENTIFIER '[' formula ']' IDENTIFIER;
automatonDecl : stateDecl ';' initialDecl ';' acceptDecl ';'
            		    transitionDecl (';' transitionDecl)*;
buchiDecl : letDecl? automatonDecl;

CONJUNCTION: 'and' | '&' | '&&' | '/\\' | '*';
DISJUNCTION: 'or' | '|' | '||' | '\\/' | '+';
EQUIVALENCE: 'iff' | '<->' | '<=>';
EVENTUALLY: 'eventually' | 'F' | '<>';
FALSE: 'false' | '0';
GLOBALLY: 'globally' | 'always' | 'G' | '[]';
IMPLICATION: 'implies' | '->' | '=>';
IN : 'in';
LET : 'let' | '\\';
NEGATION: '!' | '~' | 'not';
NEXT : 'next' | 'N' | '()';
SUNTIL: 'until' | 'U';
WUNTIL: 'W';
SRELEASE: 'M';
WRELEASE: 'R';
TRUE: 'true' | '1';
XOR: 'xor' | '^';

STATES: 'states';
INITIAL: 'initial';
ACCEPT: 'accept';

reserved: CONJUNCTION | DISJUNCTION | EQUIVALENCE | EVENTUALLY | FALSE | GLOBALLY | IMPLICATION | IN | LET | NEGATION | NEXT | SUNTIL | WUNTIL | SRELEASE | WRELEASE | TRUE | XOR;

ATOMINLINE : PIPEATOM | QUOTEATOM;
PIPEATOM : '|' ('\\|' | ~'|')* '|';
QUOTEATOM: '"' ('\"' | ~'"')* '"';

IDENTIFIER : [a-zA-Z][a-zA-Z_0-9]*;

EQ : '=';
COMMA : ',';
SEMICOLON : ';';
LPAREN : '(';
RPAREN : ')';

LINE_COMMENT : '//' .*? '\n' -> skip ;
COMMENT : '/*' .*? '*/' -> skip ;
WS : [ \r\t\n]+ -> skip ;
