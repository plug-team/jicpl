# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* JIC Property Language is a generic property specification language customizable for property specification on different modeling languages through plugins
* version 0.0.1 - initial developments
* It support property specification though predicates, LTL formulas, observer and buchi automata.
* In the future we plan to extend it with SERE, and maybe event-observer automata (like CDL observers).

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Ciprian Teodorov
* Other community or team contact