grammar LDL;

block : formulaDeclaration+;

formulaDeclaration : IDENTIFIER EQ formula;
formulaDeclarationList : formulaDeclaration (COMMA formulaDeclaration)* COMMA?;


path_formula :
          formula                                   #PropositionalFormula
        | formula '?'                               #TestTemporal
        | LPAREN path_formula RPAREN                #ParenPath
        | path_formula STAR                         #Star
        | path_formula '{' NUMBER (':' NUMBER)? '}' #Interval
        | path_formula ';' path_formula             #Sequence
        | path_formula '+' path_formula             #Alternative
        | LET 'path' formulaDeclarationList IN path_formula #LetPathExp
        ;

formula :
		  literal													                #LiteralExp
		| IDENTIFIER												                #ReferenceExp
		| atom														                #AtomExp
		| LPAREN formula RPAREN 									                #ParenExp
		| operator=NEGATION formula											        #UnaryExp
		| operator=NEXT formula                                                     #UnaryExp
		| operator=(EVENTUALLY|GLOBALLY) formula                                    #UnaryExp
		|<assoc=right> formula operator=(SUNTIL|WUNTIL|SRELEASE|WRELEASE) formula   #BinaryExp
		| formula operator=CONJUNCTION formula						                #BinaryExp
		| formula operator=DISJUNCTION formula						                #BinaryExp
		| formula operator=XOR formula								                #BinaryExp
		|<assoc=right> formula operator=(IMPLICATION | EQUIVALENCE) formula         #BinaryExp
		| '<' path_formula '>' formula                                              #EventuallyLDL
		| '[' path_formula ']' formula                                              #GloballyLDL
		| LET formulaDeclarationList IN formula					        	        #LetExp
		;


literal : TRUE | FALSE;
atom : IDENTIFIER? ATOMINLINE;

CONJUNCTION: 'and' | '&' | '&&' | '/\\';
STAR : '*';
ALTERNATIVE : '+';
DISJUNCTION: 'or' | '|' | '||' | '\\/';
EQUIVALENCE: 'iff' | '<->' | '<=>';
EVENTUALLY: 'eventually' | 'F' | '<>';
FALSE: 'false' | '0';
GLOBALLY: 'globally' | 'always' | 'G' | '[]';
IMPLICATION: 'implies' | '->' | '=>';
IN : 'in';
LET : 'let' | '\\';
NEGATION: '!' | '~' | 'not';
NEXT : 'next' | 'N' | '()';
SUNTIL: 'until' | 'U';
WUNTIL: 'W';
SRELEASE: 'M';
WRELEASE: 'R';
TRUE: 'true' | '1';
XOR: 'xor' | '^';

reserved: CONJUNCTION | DISJUNCTION | EQUIVALENCE | EVENTUALLY | FALSE | GLOBALLY | IMPLICATION | IN | LET | NEGATION | NEXT | SUNTIL | WUNTIL | SRELEASE | WRELEASE | TRUE | XOR;

ATOMINLINE : PIPEATOM | QUOTEATOM;
PIPEATOM : '|' ('\\|' | ~'|')* '|';
QUOTEATOM: '"' ('\"' | ~'"')* '"';

IDENTIFIER : [a-zA-Z][a-zA-Z_0-9]*;
NUMBER : [1-9][0-9]*;

EQ : '=';
COMMA : ',';
LPAREN : '(';
RPAREN : ')';

LINE_COMMENT : '//' .*? '\n' -> skip ;
COMMENT : '/*' .*? '*/' -> skip ;
WS : [ \r\t\n]+ -> skip ;
