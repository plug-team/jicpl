grammar PSL;

//A.4.1 Verification units
psl_specification: verification_unit*; //in Annex A.4.1 psl_specification: verification_item*;

verification_unit:
    vunit_type psl_identifier ('(' context_spec ')')? '{'
        inherit_spec*
        override_spec*
        vunit_item*
    '}'
;
vunit_type:
        'vunit'
    |   'vpkg'
    |   'vprop'
    |   'vmode'
;

context_spec:
        binding_spec
    |   formal_parameter_list
;
binding_spec: hierarchical_hdl_name;
hierarchical_hdl_name: hdl_module_name (path_separator /*instance*/name)*;
hdl_module_name: /*HDL_Module*/ name ('(' /*HDL_Module*/ name ')')?;
path_separator: '.' | '/';
/*instance*/name: hdl_or_psl_identifier;

inherit_spec: 'nontransitive'? 'inherit' /*vunit*/ name (',' /*vunit*/ name)* ';';

vunit_item:
        aHDL_DECL
    |   aHDL_STMT
    |   psl_declaration
    |   psl_directive
    |   vunit_instance
;
//missing from the annex A?. Found in 7.2.2 Verification unit instantiation
vunit_instance:
    label ':' vunit_type /*vunit*/ name ('(' actual_parameter_list ')')? ';'
;
//missing from the annex A?

override_spec: 'override' name_list;

name_list: name (',' name)*;
formal_parameter_list: formal_parameter (';' formal_parameter)*;

//A.4.2 PSL declarations
psl_declaration:
        property_declaration
    |   sequence_declaration
    |   clock_declaration
;

property_declaration: 'property' psl_identifier ('(' formal_parameter_list ')')? DEF_SYM property;
formal_parameter: param_spec psl_identifier (',' psl_identifier)*;
param_spec:
        'const'
    |   ('const' | 'mutable') value_parameter
    |   sequence
    |   property
;
value_parameter: hdl_type | psl_type_class;
hdl_type: 'hdltype' aHDL_VARIABLE_TYPE;
psl_type_class: 'boolean' | 'bit' | 'bitvector' | 'numberic' | 'string';
sequence_declaration: 'sequence' psl_identifier ('(' formal_parameter_list ')')? DEF_SYM sequence;

clock_declaration: 'default' 'clock' DEF_SYM clock_expression;
clock_expression:
        /*boolean*/ name
    |   /*boolean*/ built_in_function_call
    |   '(' boolean ')'
    |   '(' aHDL_CLOCK_EXP ')'
;

actual_parameter_list: actual_parameter (',' actual_parameter)*;
actual_parameter:
        any_type
    |   number
    |   boolean
    |   property
    |   sequence
;

//A.4.3 PSL directives, p. 142
psl_directive: '[' label ':' ']' verification_directive;
verification_directive:
        assert_directive
    |   assume_directive
    |   restrict_directive
    |   restrict_strict_directive
    |   cover_directive
    |   fairness_statement
    ;
assert_directive: ASSERT property '[' REPORT string ']' ';' ;
assume_directive: ASSUME property ';' ;
restrict_directive: RESTRICT sequence ';' ;
restrict_strict_directive: RESTRICT_STRICT sequence ';';
cover_directive: COVER sequence '[' REPORT string ']' ';' ;
fairness_statement:
    FAIRNESS boolean
    | STRONG FAIRNESS boolean ',' boolean ';'
    ;



//A.4.4 PSL properties, p.142-143
property:
        replicator property
    |   fl_property
    |   obe_property
    ;
replicator: FORALL parameter_definition ':' ;
index_range:
        LEFT_SYM /*finite*/ range RIGHT_SYM
    |   '(' /*HDL*/ range ')'
    ;
value_set:
        '{' value_range (',' value_range)* '}'
    |   'boolean'
    ;
value_range:
        value
    |   /*finite*/ range
    ;
value:
        boolean
    |   number
    ;

//this seems to be something specific HDL
proc_block: proc_block_item*;
proc_block_item:
        aHDL_DECL
    |   aHDL_SEQ_STMT
    ;

fl_property:
        boolean                                                 //6.2.1 The most basic FL Property is a Boolean expression.
    |   '(' fl_property ')'                                     //6.2.1 An FL Property enclosed in parentheses is also an FL Property.
    |   ('[[' HDL_DECL (',' HDL_DECL)* ']]')? fl_property
    |   sequence '!'?
    |   /*FL_property*/ name ('(' actual_parameter_list ')')?
    |   fl_property '@' clock_expression
    |   fl_property ABORT       boolean
    |   fl_property ASYNC_ABORT boolean
    |   fl_property SYNC_ABORT  boolean
    |   parameterized_property
    //Logical Operators
    |   NOT_OP fl_property
    |   fl_property AND_OP  fl_property
    |   fl_property OR_OP   fl_property
    |   fl_property '->'    fl_property
    |   fl_property '<->'   fl_property
//  : Primitive LTL Operators :
    |   'X'     fl_property
    |   'X!'    fl_property
    |   'F'     fl_property
    |   'G'     fl_property
    |   '[' fl_property 'U' fl_property ']'
    |   '[' fl_property 'W' fl_property ']'
//  : Simple Temporal Operators :
    |   'always'                fl_property
    |   'never'                 fl_property
    |   'next'                  fl_property
    |   'next!'                 fl_property
    |   'eventually!'           fl_property
//  :
    |   fl_property 'until!'    fl_property
    |   fl_property 'until'     fl_property
    |   fl_property 'until!_'   fl_property
    |   fl_property 'until_'    fl_property
//  :
    |   fl_property 'before!'   fl_property
    |   fl_property 'before'    fl_property
    |   fl_property 'before!_'  fl_property
    |   fl_property 'before_'   fl_property
//  : Extended Next (Event) Operators :
    |   'X'     '[' number ']' '(' fl_property ')'
    |   'X!'    '[' number ']' '(' fl_property ')'
    |   'next'  '[' number ']' '(' fl_property ')'
    |   'next!' '[' number ']' '(' fl_property ')'
//  : (see A.4.7)
    |   'next_a'    '[' /*finite*/ range ']' '(' fl_property ')'
    |   'next_a!'   '[' /*finite*/ range ']' '(' fl_property ')'
    |   'next_e'    '[' /*finite*/ range ']' '(' fl_property ')'
    |   'next_e!'   '[' /*finite*/ range ']' '(' fl_property ')'
//  :
    |   'next_event!'   '(' boolean ')' '(' fl_property ')'
    |   'next_event'    '(' boolean ')' '(' fl_property ')'
    |   'next_event!'   '(' boolean ')' '[' /*positive*/ number ']' '(' fl_property ')'
    |   'next_event'    '(' boolean ')' '[' /*positive*/ number ']' '(' fl_property ')'
//  :
    |   'next_event_a!' '(' boolean ')' '[' /*finite_positive*/ range ']' '(' fl_property ')'
    |   'next_event_a'  '(' boolean ')' '[' /*finite_positive*/ range ']' '(' fl_property ')'
    |   'next_event_e!' '(' boolean ')' '[' /*finite_positive*/ range ']' '(' fl_property ')'
    |   'next_event_e'  '(' boolean ')' '[' /*finite_positive*/ range ']' '(' fl_property ')'
//  : Operators on SEREs :
    |   sere? '(' fl_property ')'
    |   sequence '|->' fl_property
    |   sequence '|=>' fl_property
;

//A.4.5 Sequential Extended Regular Expressions (SEREs)
sere:
        boolean
    |   boolean proc_block
    |   sequence
    |   sere ';' sere
    |   sere ':' sere
    |   compound_sere
;

compound_sere:
        repeated_sere
    |   braced_sere
    |   clocked_sere
    |   compound_sere   '|'         compound_sere
    |   compound_sere   '&'         compound_sere   //non-lenght matching
    |   compound_sere   '&&'        compound_sere   //lenght matching and
    |   compound_sere   'within'    compound_sere
    |   parameterized_sere
;

//A.4.6 Parameterized Properties and SEREs
parameterized_property:
    'for' parameters_definition ':' and_or_property_OP '(' fl_property ')'
;
parameterized_sere:
    'for' parameters_definition ':' and_or_sere_OP '{' sere '}'
;
parameters_definition: parameter_definition parameter_definition*;
parameter_definition: psl_identifier index_range? 'in' value_set;
and_or_property_OP:
        AND_OP
    |   OR_OP
;
and_or_sere_OP:
        '&&'
    |   '&'
    |   '|'
;

//A.4.7 Sequences
sequence:
        sequence_instance
    |   repeated_sere
    |   braced_sere
    |   clocked_sere
    |   sequence proc_block
;

repeated_sere:
        boolean     '[*' count? ']'
    |   sequence    '[*' count? ']'
    |   '[*' count? ']'
    |   boolean     '[+]'
    |   sequence    '[+]'
    |   '[+]'
    |   boolean     '[=' count ']'
    |   boolean     '[->' /*positive*/ count? ']'
    |   boolean     proc_block
    |   sequence    proc_block
;

braced_sere:
        '{' ('[[' HDL_DECL+ ']]')? sere '}'
    |   '{' ('free' hdl_identifier+)? sere '}'
;

sequence_instance: /*sequence*/ name ('(' actual_parameter_list ')')?;
clocked_sere: braced_sere '@' clock_expression;
count:
        number
    |   range
;

range: low_bound RANGE_SYM high_bound;
low_bound:
        number
    |   MIN_VAL
;
high_bound:
        number
    |   MAX_VAL
;

//A.4.8 Forms of expression

any_type:               hdl_or_psl_expression;
bit:      /*bit*/       hdl_or_psl_expression;
boolean:  /*boolean*/   hdl_or_psl_expression;
bitvector:/*bitvector*/ hdl_or_psl_expression;
number:   /*numeric*/   hdl_or_psl_expression;
string:   /*string*/    hdl_or_psl_expression;

hdl_or_psl_expression:
        aHDL_EXP
    |   psl_expression
    |   built_in_function_call
    |   union_expression
;

psl_expression:
        boolean '->'    boolean
    |   boolean '<->'   boolean
;

built_in_function_call:
        'prev'          '(' any_type    (',' number (',' clock_expression)?)?   ')'
    |   'next'          '(' any_type                                            ')'
    |   'stable'        '(' any_type    (',' clock_expression)?                 ')'
    |   'rose'          '(' bit         (',' clock_expression)?                 ')'
    |   'fell'          '(' bit         (',' clock_expression)?                 ')'
    |   'ended'         '(' sequence    (',' clock_expression)?                 ')'
    |   'isunknown'     '(' bitvector                                           ')'
    |   'countones'     '(' bitvector                                           ')'
    |   'onehot'        '(' bitvector                                           ')'
    |   'onehot0'       '(' bitvector                                           ')'
    |   'nondet'        '(' value_set                                           ')'
    |   'nondet_vector' '(' number       ',' value_set                          ')'
;
union_expression: any_type 'union' any_type;            //a=b union c is a non-deterministic assignment of either b or c to variable or signal a.

//A.4.9 Optional Branching Extension

obe_property:
        boolean
    |   '(' obe_property ')'
    |   /*obe_property*/ name ('(' actual_parameter_list ')')?
//  : Logical Operators :
    |   NOT_OP obe_property
    |   obe_property AND_OP obe_property
    |   obe_property OR_OP  obe_property
    |   obe_property '->'   obe_property
    |   obe_property '<->'  obe_property
//  : Universal Operators :
    |   'AX' obe_property
    |   'AG' obe_property
    |   'AF' obe_property
    |   'A' '[' obe_property 'U' obe_property ']'
//  : Existential Operators :
    |   'EX' obe_property
    |   'EG' obe_property
    |   'EF' obe_property
    |   'E' '[' obe_property 'U' obe_property ']'
;




ABORT: 'abort';
ASYNC_ABORT: 'async_abort';
SYNC_ABORT: 'sync_abort';
ASSERT: 'assert';
ASSUME: 'assume';
RESTRICT: 'restrict';
RESTRICT_STRICT: 'restrict!';
COVER: 'cover';
FAIRNESS: 'fairness';
STRONG: 'strong';
REPORT: 'report';
BOOLEAN: 'boolean';


DEF_SYM: ':=' | 'is';
RANGE_SYM: 'to' | '..';
AND_OP: '&&' | 'and';
OR_OP: '||' | 'or';
NOT_OP: '!' | 'not' | '~';
MIN_VAL: '0';
MAX_VAL: 'inf';
LEFT_SYM: '[';
RIGHT_SYM: ']';



label: psl_identifier; //TODO
hdl_or_psl_identifier: hdl_identifier | psl_identifier;
psl_identifier: 'identifier'; //? //TODO
hdl_identifier: 'hdl_ident'; //? //TODO
aHDL_DECL: 'hdl_decl';//TODO
aHDL_STMT: 'hdl_stmt';//TODO
aHDL_SEQ_STMT: 'hdl_seq_stmt';
aHDL_VARIABLE_TYPE: 'hdl_var_type'; //TODO
aHDL_CLOCK_EXP: 'hdl_clock'; //TODO
aHDL_EXP: 'hdl_exp';

