package properties.LDL.parser;

public class Parser {
/*	private static Parser instance = new Parser();

    public static Expression parse(File file) throws IOException {
        ANTLRFileStream fs = new ANTLRFileStream(file.getAbsolutePath());

        return instance.parse(fs, Expression.class);
    }

    public static Expression parse(String program) {
        ANTLRInputStream is = new ANTLRInputStream(program);

        return instance.parse(is, Expression.class);
    }
    
    public static DeclarationBlock parseBlock(File file) throws IOException {
        ANTLRFileStream fs = new ANTLRFileStream(file.getAbsolutePath());

        return instance.parse(fs, DeclarationBlock.class);
    }

    public static DeclarationBlock parseBlock(String program) {
        ANTLRInputStream is = new ANTLRInputStream(program);

        return instance.parse(is, DeclarationBlock.class);
    }

    public static URI generateModelXMI(Expression exp, String fileName) throws IOException {
        return instance.saveXMI(exp, fileName);
    }
	
	
	public <T extends Element> T parse(ANTLRInputStream is, Class<T> type) {
        LTLLexer lexer = new LTLLexer(is);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        LTLParser parser = new LTLParser(tokens);
        ParseTree tree;
        if (type.isAssignableFrom(DeclarationBlock.class)) {
        	tree = parser.block();
        }
        else {
        	tree = parser.formula();
        }
        ParseTreeWalker walker = new ParseTreeWalker();

        ASTBuilder builder = new ASTBuilder();

        //TODO define a clear error handling strategy for Parsing
        parser.removeErrorListeners();
        parser.addErrorListener(new ErrorHandler());

        try {
            walker.walk(builder, tree);
            SymbolResolver resolver = new SymbolResolver(builder.values);
            walker.walk(resolver, tree);
            return resolver.getValue(tree, type);

        } catch (Error e) {
            java.lang.System.err.println(e.getMessage());
            return null;
        }
    }
	
	public URI saveXMI(Element object, String fileName) throws IOException {
        ResourceSet resourceSet = new ResourceSetImpl();
        resourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap().put("xmi", new XMIResourceFactoryImpl());
        Resource r = resourceSet.createResource(URI.createFileURI(fileName));
        r.getContents().add(object);
        r.save(Collections.emptyMap());
        return r.getURI();
    }
	
	public static class ErrorHandler extends BaseErrorListener {
        @Override
        public void syntaxError(Recognizer<?, ?> rec, Object offendingSymbol, int line, int column, String msg, RecognitionException e) {
            throw new RuntimeException(e.getLocalizedMessage());
        }
    }
    */
}
