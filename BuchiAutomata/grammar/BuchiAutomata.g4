grammar BuchiAutomata;

block : buchiDeclaration+;

buchiDeclaration : IDENTIFIER EQ (LET formulaDeclarationList IN)? automaton;

automaton :
	initialDeclaration
	acceptDeclaration
	transitionDeclaration*
;

initialDeclaration : INITIAL IDENTIFIER SEMICOLON;
acceptDeclaration: ACCEPT IDENTIFIER+ SEMICOLON;
transitionDeclaration: source=IDENTIFIER LSQUARE formula RSQUARE target=IDENTIFIER SEMICOLON;

formulaDeclaration : IDENTIFIER EQ formula;
formulaDeclarationList : formulaDeclaration (COMMA formulaDeclaration)* COMMA?;

formula : 
		  literal													        #LiteralExp
		| IDENTIFIER												        #ReferenceExp
		| atom														        #AtomExp
		| LPAREN formula RPAREN 									        #ParenExp
		| NEGATION formula											        #UnaryExp
		| formula operator=CONJUNCTION formula						        #BinaryExp
		| formula operator=DISJUNCTION formula						        #BinaryExp
		| formula operator=XOR formula								        #BinaryExp
		|<assoc=right> formula operator=(IMPLICATION | EQUIVALENCE) formula #BinaryExp
		| LET formulaDeclarationList IN formula					        	#LetExp
		;

literal : TRUE | FALSE;
atom : IDENTIFIER? ATOMINLINE;

ACCEPT: 'accept';
CONJUNCTION: 'and' | '&' | '&&' | '/\\' | '*';
DISJUNCTION: 'or' | '|' | '||' | '\\/' | '+';
EQUIVALENCE: 'iff' | '<->' | '<=>';
FALSE: 'false' | '0';
IMPLICATION: 'implies' | '->' | '=>';
IN : 'in';
INITIAL : 'initial';
LET : 'let' | '\\';
NEGATION: '!' | '~' | 'not';
TRUE: 'true' | '1';
XOR: 'xor' | '^';

reserved: CONJUNCTION | DISJUNCTION | EQUIVALENCE | FALSE | IMPLICATION | IN | LET | NEGATION | TRUE | XOR;

ATOMINLINE : PIPEATOM | QUOTEATOM;
PIPEATOM : '|' ('\\|' | ~'|')* '|';
QUOTEATOM: '"' ('\"' | ~'"')* '"';

IDENTIFIER : [a-zA-Z][a-zA-Z_0-9]*;

EQ : '=';
COMMA : ',';
LPAREN : '(';
RPAREN : ')';
SEMICOLON: ';';
LSQUARE : '[';
RSQUARE : ']';

LINE_COMMENT : '//' .*? '\n' -> skip ;
COMMENT : '/*' .*? '*/' -> skip ;
WS : [ \r\t\n]+ -> skip ;