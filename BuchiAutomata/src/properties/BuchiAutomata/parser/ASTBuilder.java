package properties.BuchiAutomata.parser;

import org.antlr.v4.runtime.tree.ParseTreeProperty;
import org.antlr.v4.runtime.tree.TerminalNode;
import properties.BuchiAutomata.BuchiAutomataModel.DeclarationBlock;
import properties.BuchiAutomata.BuchiAutomataModel.*;
import properties.BuchiAutomata.grammar.BuchiAutomataBaseListener;
import properties.BuchiAutomata.grammar.BuchiAutomataParser;
import properties.BuchiAutomata.grammar.BuchiAutomataParser.*;
import properties.PropositionalLogic.PropositionalLogicModel.*;
import properties.PropositionalLogic.parser.TreeAnnotator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ASTBuilder extends BuchiAutomataBaseListener implements TreeAnnotator {
	public ParseTreeProperty<Object> values = new ParseTreeProperty<>();
	public ParseTreeProperty<Object> getValues() {
		return values;
	};
	
	PropositionalLogicModelFactory expFactory = PropositionalLogicModelFactory.eINSTANCE;
	BuchiAutomataModelFactory factory = BuchiAutomataModelFactory.eINSTANCE;

	@Override
    public void exitLiteral(LiteralContext ctx) {    
    	//ideally true and false should be singletons, but there are containment problems
        setValue(ctx, ctx.TRUE() == null ? expFactory.createFalse() : expFactory.createTrue());
    }
    
    @Override
    public void exitAtom(AtomContext ctx) {
    	Atom atom = expFactory.createAtom();
    	String language = ctx.IDENTIFIER() != null ? ctx.IDENTIFIER().getText() : null;
    	atom.setLanguage(language);
    	String code = ctx.ATOMINLINE().getText();
    	//remove the '|' or '"' at the beginning and at the end
    	atom.setCode(code.substring(1, code.length()-1));
    	
    	atom.setDelimiterString(code.substring(0,1));
    	setValue(ctx, atom);
    }
    
    @Override
    public void exitParenExp(ParenExpContext ctx) {
    	setValue(ctx, getValue(ctx.formula()));
    }
    
    @Override
    public void exitUnaryExp(UnaryExpContext ctx) {
    	LogicalNegation value = expFactory.createLogicalNegation();
    	value.setOperatorToken(ctx.NEGATION().getText());
    	value.setOperand(getValue(ctx.formula(), Expression.class));
    	setValue(ctx, value);
    }
    
    @Override
    public void exitBinaryExp(BinaryExpContext ctx) {
    	switch (ctx.operator.getType()) {
			case BuchiAutomataParser.CONJUNCTION: {
				LogicalConjunction value = expFactory.createLogicalConjunction();
				value.setOperatorToken(ctx.operator.getText());
				value.setLhs(getValue(ctx.formula(0), Expression.class));
				value.setRhs(getValue(ctx.formula(1), Expression.class));
				setValue(ctx, value);
    			return;
			}
			case BuchiAutomataParser.DISJUNCTION: {
				LogicalDisjunction value = expFactory.createLogicalDisjunction();
				value.setOperatorToken(ctx.operator.getText());
				value.setLhs(getValue(ctx.formula(0), Expression.class));
				value.setRhs(getValue(ctx.formula(1), Expression.class));
				setValue(ctx, value);
				return;
			}
			case BuchiAutomataParser.XOR: {
				LogicalXOR value = expFactory.createLogicalXOR();
				value.setOperatorToken(ctx.operator.getText());
				value.setLhs(getValue(ctx.formula(0), Expression.class));
				value.setRhs(getValue(ctx.formula(1), Expression.class));
				setValue(ctx, value);
				return;
			}
			case BuchiAutomataParser.IMPLICATION: {
				LogicalImplication value = expFactory.createLogicalImplication();
				value.setOperatorToken(ctx.operator.getText());
				value.setLhs(getValue(ctx.formula(0), Expression.class));
				value.setRhs(getValue(ctx.formula(1), Expression.class));
				setValue(ctx, value);
				return;
			}
			case BuchiAutomataParser.EQUIVALENCE: {
				LogicalEquivalence value = expFactory.createLogicalEquivalence();
				value.setOperatorToken(ctx.operator.getText());
				value.setLhs(getValue(ctx.formula(0), Expression.class));
				value.setRhs(getValue(ctx.formula(1), Expression.class));
				setValue(ctx, value);
				return;
			}
			default: 
				throw new RuntimeException("unexpected binary operator: " + ctx.operator.getText() + " (line: "+ ctx.getStart().getLine() +")\n");
    	}
    }
    
    @Override
    public void exitFormulaDeclaration(FormulaDeclarationContext ctx) {
    	ExpressionDeclaration value = expFactory.createExpressionDeclaration();
    	value.setName(ctx.IDENTIFIER().getText());
    	value.setExpression(getValue(ctx.formula(), Expression.class));
    	setValue(ctx, value);
    }
    
    @Override
    public void exitFormulaDeclarationList(FormulaDeclarationListContext ctx) {
    	List<ExpressionDeclaration> value = new ArrayList<ExpressionDeclaration>();
    	
    	for (FormulaDeclarationContext fctx : ctx.formulaDeclaration()) {
    		value.add(getValue(fctx, ExpressionDeclaration.class));
    	}
    	setValue(ctx, value);
    }
    
    @Override
    public void exitLetExp(LetExpContext ctx) {
    	LetExpression value = expFactory.createLetExpression();

    	@SuppressWarnings("unchecked")
		List<ExpressionDeclaration> declarations = getValue(ctx.formulaDeclarationList(), List.class);
    	value.getDeclarations().addAll(declarations);
    	
    	value.setExpression(getValue(ctx.formula(), Expression.class));
    	
    	setValue(ctx, value);
    }
    
    @Override
    public void exitBlock(BlockContext ctx) {
    	DeclarationBlock value = factory.createDeclarationBlock();
    	
    	for (BuchiDeclarationContext fctx : ctx.buchiDeclaration()) {
    		BuchiDeclaration decl = getValue(fctx, BuchiDeclaration.class);
    		value.getDeclarations().add(decl);
    	}
    	
    	setValue(ctx, value);
    }
    
    @Override
    public void exitAtomExp(AtomExpContext ctx) {
    	setValue(ctx, getValue(ctx.atom()));
    }
    
    @Override
    public void exitLiteralExp(LiteralExpContext ctx) {
    	setValue(ctx, getValue(ctx.literal()));
    }
    
    @Override
    public void exitReferenceExp(ReferenceExpContext ctx) {
    	ExpressionReference value = expFactory.createExpressionReference();
    	value.setName(ctx.IDENTIFIER().getText());
    	setValue(ctx, value);
    }
    Map<String, State> automatonStates;
    @Override
    public void enterAutomaton(AutomatonContext ctx) {
    	automatonStates = new HashMap<String, State>();
    }
    
    @Override
    public void exitInitialDeclaration(InitialDeclarationContext ctx) {
    	State state = getState(ctx.IDENTIFIER().getText());
    	setValue(ctx, state);
    }
    
    @Override
    public void exitAcceptDeclaration(AcceptDeclarationContext ctx) {
    	for (TerminalNode term : ctx.IDENTIFIER()) {
    		if (automatonStates.containsKey(term.getText())) {
    			throw new RuntimeException("State redefinition error : " + term.getText());
    		}
    		AcceptingState acceptState = factory.createAcceptingState();
    		acceptState.setName(term.getText());
    		automatonStates.put(term.getText(), acceptState);
    	}
    	//do not add these to the annotated tree
    }
    
    private State getState(String name) {
    	State state = automatonStates.get(name);
    	if (state == null) {
    		state = factory.createState();
    		state.setName(name);
    		automatonStates.put(name, state);
    	}
    	return state;
    }
    
    @Override
    public void exitTransitionDeclaration(TransitionDeclarationContext ctx) {
    	State sourceState = getState(ctx.source.getText());
    	State targetState = getState(ctx.target.getText());
    	GuardedTransition transition = factory.createGuardedTransition();
    	transition.setFrom(sourceState);
    	transition.setGuard(getValue(ctx.formula(), Expression.class));
    	transition.setTo(targetState);
    	setValue(ctx, transition);
    }
    
    @Override
    public void exitAutomaton(AutomatonContext ctx) {
    	Automaton value = factory.createAutomaton();
    	
    	value.setInitial(getValue(ctx.initialDeclaration(), State.class));
    	value.getStates().addAll(automatonStates.values());
    	
    	for (TransitionDeclarationContext transitionCtx : ctx.transitionDeclaration()) {
    		GuardedTransition transition = getValue(transitionCtx, GuardedTransition.class);
    		value.getTransitions().add(transition);
    	}
    	
    	setValue(ctx, value);
    }
    
    @SuppressWarnings("unchecked")
	@Override
    public void exitBuchiDeclaration(BuchiDeclarationContext ctx) {
    	BuchiDeclaration value = factory.createBuchiDeclaration();
    	
    	value.setName(ctx.IDENTIFIER().getText());
    	
    	if (ctx.formulaDeclarationList() != null) {
    		value.getDeclarations().addAll(getValue(ctx.formulaDeclarationList(), List.class));
    	}
    	value.setAutomaton(getValue(ctx.automaton(), Automaton.class));
    	
    	setValue(ctx, value);
    }
    
}
