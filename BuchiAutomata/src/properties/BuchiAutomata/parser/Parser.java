package properties.BuchiAutomata.parser;

import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;
import properties.BuchiAutomata.BuchiAutomataModel.BuchiDeclaration;
import properties.BuchiAutomata.grammar.BuchiAutomataLexer;
import properties.BuchiAutomata.grammar.BuchiAutomataParser;

import java.io.File;
import java.io.IOException;
import java.util.Collections;

public class Parser {
    private static Parser instance = new Parser();

    public static BuchiDeclaration parse(File file) throws IOException {
        ANTLRFileStream fs = new ANTLRFileStream(file.getAbsolutePath());

        return instance.parse(fs);
    }

    public static BuchiDeclaration parse(String program) {
        ANTLRInputStream is = new ANTLRInputStream(program);

        return instance.parse(is);
    }

    public static URI generateModelXMI(BuchiDeclaration exp, String fileName) throws IOException {
        return instance.saveXMI(exp, fileName);
    }


    public BuchiDeclaration parse(ANTLRInputStream is) {
        BuchiAutomataLexer lexer = new BuchiAutomataLexer(is);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        BuchiAutomataParser parser = new BuchiAutomataParser(tokens);
        ParseTree tree;

        tree = parser.buchiDeclaration();

        ParseTreeWalker walker = new ParseTreeWalker();

        ASTBuilder builder = new ASTBuilder();

        //TODO define a clear error handling strategy for Parsing
        parser.removeErrorListeners();
        parser.addErrorListener(new ErrorHandler());

        try {
            walker.walk(builder, tree);
            //SymbolResolver resolver = new SymbolResolver(builder.values);
            //walker.walk(resolver, tree);
            return builder.getValue(tree, BuchiDeclaration.class);//resolver.getValue(tree, type);

        } catch (Error e) {
            java.lang.System.err.println(e.getMessage());
            return null;
        }
    }

    public URI saveXMI(BuchiDeclaration object, String fileName) throws IOException {
        ResourceSet resourceSet = new ResourceSetImpl();
        resourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap().put("xmi", new XMIResourceFactoryImpl());
        Resource r = resourceSet.createResource(URI.createFileURI(fileName));
        r.getContents().add(object);
        r.save(Collections.emptyMap());
        return r.getURI();
    }

    public static class ErrorHandler extends BaseErrorListener {
        @Override
        public void syntaxError(Recognizer<?, ?> rec, Object offendingSymbol, int line, int column, String msg, RecognitionException e) {
            throw new RuntimeException(e.getLocalizedMessage());
        }
    }
}
