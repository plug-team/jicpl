package properties.BuchiAutomata.analysis;

import org.eclipse.emf.ecore.EObject;
import properties.BuchiAutomata.BuchiAutomataModel.Automaton;
import properties.BuchiAutomata.BuchiAutomataModel.BuchiDeclaration;
import properties.BuchiAutomata.BuchiAutomataModel.GuardedTransition;
import properties.BuchiAutomata.BuchiAutomataModel.util.BuchiAutomataModelSwitch;
import properties.PropositionalLogic.PropositionalLogicModel.Atom;
import properties.PropositionalLogic.PropositionalLogicModel.Declaration;

import java.util.*;

public class AtomExtractor {

    public List<Atom> getAtoms(BuchiDeclaration buchiDeclaration) {
        Extractor extractor = new Extractor();
        extractor.doSwitch(buchiDeclaration);
        return extractor.atoms;
    }

    class Extractor extends BuchiAutomataModelSwitch<Boolean> {
        properties.PropositionalLogic.transformations.AtomExtractor extractor = new properties.PropositionalLogic.transformations.AtomExtractor();
        Set<EObject> known = Collections.newSetFromMap(new IdentityHashMap<>());
        List<Atom> atoms = new ArrayList<>();

        @Override
        public Boolean caseAutomaton(Automaton object) {
            if (known.contains(object)) {
                return true;
            }
            for (GuardedTransition transition : object.getTransitions()) {
                atoms.addAll(extractor.getAtoms(transition.getGuard(), known));
            }
            known.add(object);
            return true;
        }

        @Override
        public Boolean caseBuchiDeclaration(BuchiDeclaration object) {
            if (known.contains(object)) {
                return true;
            }

            for (Declaration decl : object.getDeclarations()) {
                atoms.addAll(extractor.getAtoms(decl, known));
            }

            doSwitch(object.getAutomaton());

            known.add(object);
            return true;
        }
    }
}
