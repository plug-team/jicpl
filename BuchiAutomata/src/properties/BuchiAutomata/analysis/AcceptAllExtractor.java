package properties.BuchiAutomata.analysis;

import properties.BuchiAutomata.BuchiAutomataModel.AcceptingState;
import properties.BuchiAutomata.BuchiAutomataModel.Automaton;
import properties.BuchiAutomata.BuchiAutomataModel.BuchiDeclaration;
import properties.BuchiAutomata.BuchiAutomataModel.GuardedTransition;
import properties.PropositionalLogic.interpreter.EvaluatorLazyStatic;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class AcceptAllExtractor {

    public static Set<Object> getAccepAllSet(BuchiDeclaration declaration) {
        return new AcceptAllExtractor().getAcceptAllSet(declaration);
    }

    public Set<Object> getAcceptAllSet(BuchiDeclaration buchiDeclaration) {
        Automaton ba = (Automaton) buchiDeclaration.getAutomaton();
        Set<AcceptingState> acceptingStates = getAcceptingStates(ba);
        return acceptingStates.stream().filter(as -> isAcceptAll(ba, as)).collect(Collectors.toSet());
    }

    Set<AcceptingState> getAcceptingStates(Automaton ba) {
        return ba.getStates()
                .stream()
                .filter(s -> s instanceof AcceptingState)
                .map(s -> (AcceptingState)s)
                .collect(Collectors.toSet());
    }

    boolean isAcceptAll(Automaton ba, AcceptingState acceptingState) {
        List<GuardedTransition> fanout = ba.getTransitions()
                .stream()
                .filter(t -> t.getFrom() == acceptingState)
                .collect(Collectors.toList());

        if (fanout.size() != 1) return false;

        GuardedTransition t = fanout.get(0);
        if (t.getFrom() != t.getTo()) return false;

        EvaluatorLazyStatic evaluator = new EvaluatorLazyStatic();
        boolean isConstantTrueGuard = false;
        try {
            isConstantTrueGuard = evaluator.evaluate(t.getGuard());
        } catch (UnsupportedOperationException e) {
            isConstantTrueGuard = false;
        }
        return isConstantTrueGuard;
    }
}
