package properties.BuchiAutomata.parser;

import org.junit.Test;
import properties.BuchiAutomata.BuchiAutomataModel.BuchiDeclaration;

import static org.junit.Assert.assertEquals;

public class ParserTest {

    @Test
    public void test1() {
        String buchi = "b1 = let\n" +
                "\t\ta1 = dve|P_1!=wait|,\n" +
                "\t\ta2 = dve|P_0=choose|,\n" +
                "\t\ta3 = dve|P_2!=choose|,\n" +
                "\t\ta4 = dve|number[1]<0|,\n" +
                "\t\tp1 = !a3 and !a4,\n" +
                "\t\tp2 = a3 and a4,\n" +
                "\t\tp1 = p1 or p2,\n" +
                "\tin\n" +
                "\t    initial s0;\n" +
                "\t\taccept s2 s3;\n" +
                "\t\ts0 [!a2 and !a1] s1;\n" +
                "\t\ts0 [(!a1 and p1) or (!a1 and p2)] s2;\n" +
                "\t\ts0 [a1] s3;\n" +
                "\t\ts1 [!a2] s1;\n" +
                "\t\ts1 [p1] s2;\n" +
                "\t\ts2 [p1] s2;\n" +
                "\t\ts3 [true] s3;";
        BuchiDeclaration decl = Parser.parse(buchi);
        System.out.println("" + decl);
        assertEquals("b1", decl.getName());
    }
}
