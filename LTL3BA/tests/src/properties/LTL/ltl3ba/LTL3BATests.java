package properties.LTL.ltl3ba;

import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class LTL3BATests {
	@Test
	public void testGloballyP() {	
		assertTrue(LTL3BA.transform("G p").size() == 1);
	}

    @Test
    public void testGloballyPandQ() {
        assertTrue(LTL3BA.transform("G p && q").size() == 2);
    }
}
