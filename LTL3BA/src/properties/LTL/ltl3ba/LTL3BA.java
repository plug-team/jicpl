package properties.LTL.ltl3ba;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

public class LTL3BA {


	final static private Path ltl3bPath;

	static {
		// Extracts ltl3ba executable from resources to be able to run it.
		String platform = System.getProperty("os.name").toLowerCase();
		String resource = "/properties/LTL/ltl3ba/exe/";
		String suffix = "";
		if (platform.contains("mac")) {
			resource += "macos/ltl3ba";
			suffix = "ltl3ba";
		} else if (platform.contains("linux")) {
			resource += "linux/ltl3ba";
			suffix = "ltl3ba";
		} else {
			// lets consider that it windows .....
			resource += "windows/ltl3ba.exe";
			suffix = "ltl3ba.exe";
		}

		Path executablePath = null;

		// Creates a tmp file on the computer
		try (InputStream inStream = LTL3BA.class.getResourceAsStream(resource)){
			executablePath = Files.createTempFile("plug", suffix);

			try (OutputStream outStream = Files.newOutputStream(executablePath)) {
				byte[] buffer = new byte[1024*10];
				int len = inStream.read(buffer);
				while (len != -1) {
					outStream.write(buffer, 0, len);
					len = inStream.read(buffer);
				}
			}

			executablePath.toFile().setExecutable(true, false);

		} catch (IOException e) {
			System.err.println("Can't create temp file to extract ltl3ba");
		}

		ltl3bPath = executablePath;
	}



	private List<String[]> ltl3ba(String formula) {
		List<String> command = new ArrayList<>();
		command.add( ltl3bPath.toString() );
		command.add("-T3");
		command.add("-f");
		
		command.add(formula);
		//System.out.println(formula);
		try {
			Process process = new ProcessBuilder(command).start();
			List<String[]> transitions = processResult(process.getInputStream());
			return transitions;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	List<String[]> processResult(InputStream is) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(is));
		//discard the acc="1" line
		br.readLine();
		String line;
		List<String[]> transitions = new ArrayList<String[]>();
		while ((line = br.readLine()) != null) {
			String parts[] = line.split(",|;");
			String guard = parts[2].trim().replaceAll("\"$|^\"", "");
			//if the guard string is (1) then it means that it is true
			if (guard.equals("(1)")) { guard = "true"; }
			transitions.add(new String[]{parts[0].trim(), parts[1].trim(), guard });
            //System.out.println(Arrays.toString(transitions.get(transitions.size()-1)));
			//the accepting transition is dropped parts[3]
		}
		
		br.close();
		return transitions;
	}
	
	public static List<String[]> transform(String formula) {
		LTL3BA tr = new LTL3BA();
		return tr.ltl3ba(formula);
	}
}

